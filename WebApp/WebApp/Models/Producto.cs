﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Producto
    {
        [Key]
        public int id_producto { get; set; }
        public string descripcion { get; set; }
        public int id_marca_producto { get; set; }
        public int id_modelo_producto { get; set; }
        public string observaciones { get; set; }
        public decimal volumen_cm { get; set; }
        public decimal peso_real { get; set; }
        public int valor_unitario { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
