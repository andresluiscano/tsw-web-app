﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class SLATransporte
    {
        [Key]
        public long id_sla_transporte { get; set; }
        //public int compromiso_entrega { get; set; }
        public int id_proveedor_transporte { get; set; }
        public string codigo_postal { get; set; }
        public int escala { get; set; }      
        //public long id_localidad { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
