﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class ContactoSucursal
    {
        [Key]
        public int id_contacto_sucursal { get; set; }
        public int id_sucursal_cliente { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        /*public string mail { get; set; }
        public int? telefono { get; set; }*/
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
