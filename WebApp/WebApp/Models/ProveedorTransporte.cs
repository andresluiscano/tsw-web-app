﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class ProveedorTransporte
    {
        [Key]
        public int id_proveedor_transporte { get; set; }
        public string descripcion { get; set; }
        public string logo_proveedor { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
        public decimal valor_maximo_permitido { get; set; }
        public decimal valor_por_remito { get; set; }
    }
}