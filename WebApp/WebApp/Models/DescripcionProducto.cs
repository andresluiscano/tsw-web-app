﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class DescripcionProducto
    {
        public long IdPedidoProducto { get; set; }
        public string Producto { get; set; }
        public string Modelo { get; set; }
        public string Marca { get; set; }
        public string Configuracion { get; set; }
        public int Cantidad { get; set; }
        public int CantidadRetorno { get; set; }
        public int CantidadPurga { get; set; }
    }
}
