﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class CodigoPostalSLA
    {
        [Key]
        public long id_codigo_postal_sla { get; set; }
        public int codigo_postal { get; set; }
        public int sla { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
