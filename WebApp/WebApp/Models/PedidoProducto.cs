﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class PedidoProducto
    {
        [Key]
        public long id_pedido_producto { get; set; }
        public long id_pedido { get; set; }
        public int id_producto { get; set; }
        public int? id_configuracion_producto { get; set; }
        public int cantidad { get; set; }
        public int cantidad_retorno { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
