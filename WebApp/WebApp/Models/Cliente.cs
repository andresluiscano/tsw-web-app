﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Cliente
    {
        [Key]
        public int id_cliente { get; set; }
        public string descripcion { get; set; }
        public string logo_cliente { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
