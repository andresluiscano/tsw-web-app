﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class CircuitoRadial
    {
        [Key]
        public int id_circuito_radial { get; set; }
        public int id_proveedor_transporte { get; set; }
        public int escala { get; set; }
        public decimal valor { get; set; }
        public decimal valor_adicional { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
