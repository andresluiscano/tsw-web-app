﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class EstadoPedido
    {
        [Key]
        public int id_estado_pedido { get; set; }
        public string descripcion { get; set; }
        public string observaciones { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
