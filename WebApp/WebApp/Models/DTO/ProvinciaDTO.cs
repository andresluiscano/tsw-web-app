﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ProvinciaDTO
    {
        private readonly Provincia _p = new Provincia();

        public ProvinciaDTO() { 
        
        }

        public ProvinciaDTO(Provincia provincia) {
            _p = provincia;
        }

        public Provincia GetProvincia() => _p;

        public bool EsNuevo() => Id == 0;

        public int? Id {
            get { return _p.id_provincia; }
            set { _p.id_provincia = value ?? 0; }
        }

        public string Descripcion
        {
            get { return _p.descripcion; }
            set { _p.descripcion = value; }
        }

        public double Latitud
        {
            get { return _p.latitud; }
            set { _p.latitud = value; }
        }

        public double Longitud
        {
            get { return _p.longitud; }
            set { _p.longitud = value; }
        }
    }
}
