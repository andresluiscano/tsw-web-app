﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class SucursalDTO
    {
        private readonly SucursalClienteInterno _sc = new SucursalClienteInterno();

        public SucursalDTO()
        {

        }

        /*public SucursalDTO(SucursalCliente sc)
        {
            _sc = sc;
        }*/

        public SucursalDTO(SucursalClienteInterno sc)
        {
            _sc = sc;
        }

        public SucursalCliente GetSucursalCliente() => _sc;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _sc.id_sucursal_cliente; }
            set { _sc.id_sucursal_cliente = value ?? 0; }
        }
        public int IdCliente
        {
            get { return _sc.id_cliente; }
            set { _sc.id_cliente = value; }
        }

        [StringLength(50)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string DescripcionSucursal
        {
            get { return _sc.descripcion; }
            set { _sc.descripcion = value; }
        }

        [Range(1, long.MaxValue, ErrorMessage = "El campo localidad no puede estar vacío.")]
        public long IdLocalidad
        {
            get { return _sc.id_localidad; }
            set { _sc.id_localidad = value; }
        }

        [StringLength(200)]
        [Required(ErrorMessage = "El campo dirección no puede estar vacío.")]
        public string Direccion
        {
            get { return _sc.direccion; }
            set { _sc.direccion = value; }
        }

        [StringLength(10)]
        [Required(ErrorMessage = "El campo código postal no puede estar vacío.")]
        public string CodigoPostal
        {
            get { return _sc.codigo_postal; }
            set { _sc.codigo_postal = value; }
        }

        public string FF_Baja
        {
            get { return _sc.ff_baja.HasValue ? _sc.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _sc.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool EsBaja { get; set; }

        public int IdProvincia
        {
            get { return _sc.id_provincia;  }
            set { _sc.id_provincia = value; }
        }

        public int? Sla
        {
            get { return _sc.sla; }
            set { _sc.sla = value; }
        }

        public int? IdCodigoPostalSla
        {
            get { return _sc.sla; }
            set { _sc.sla = value; }
        }
    }
}
