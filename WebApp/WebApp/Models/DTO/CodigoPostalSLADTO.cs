﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class CodigoPostalSLADTO
    {
        private readonly CodigoPostalSLA _cpsla = new CodigoPostalSLA();

        public CodigoPostalSLADTO()
        {

        }

        public CodigoPostalSLADTO(CodigoPostalSLA codPosSLA)
        {
            _cpsla = codPosSLA;
        }

        public CodigoPostalSLA GetCodigoPostalSLA() => _cpsla;

        public bool EsNuevo() => Id == 0;

        public long? Id
        {
            get { return _cpsla.id_codigo_postal_sla; }
            set { _cpsla.id_codigo_postal_sla = value ?? 0; }
        }

        public int CodigoPostal
        {
            get { return _cpsla.codigo_postal; }
            set { _cpsla.codigo_postal = value; }
        }

        public int Sla
        {
            get { return _cpsla.sla; }
            set { _cpsla.sla = value; }
        }
        public string FFAlta
        {
            get { return _cpsla.ff_alta.ToString("dd-MM-yyyy"); }
            set { _cpsla.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        //public string ff_baja
        //{
        //    get { return _cpsla.ff_baja.HasValue ? _cpsla.ff_baja.Value.ToString("dd/MM/yyyy") : null; }
        //    set { _cpsla.ff_baja = DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture); }
        //}
        public string FFBaja
        {
            get { return _cpsla.ff_baja.HasValue && _cpsla.ff_baja != DateTime.MinValue ? _cpsla.ff_baja.Value.ToString("dd/MM/yyyy") : ""; }
            set { _cpsla.ff_baja = value != null && value != DateTime.MinValue.ToString() ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture) : (DateTime?) null; }
        }
    }
}
