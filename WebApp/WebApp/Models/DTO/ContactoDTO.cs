﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ContactoDTO
    {
        private readonly ContactoSucursal _cs = new ContactoSucursal();

        public ContactoDTO()
        {

        }
        public ContactoDTO(ContactoSucursal cs)
        {
            _cs = cs;
        }
        public ContactoSucursal GetContactoSucursal() => _cs;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _cs.id_contacto_sucursal; }
            set { _cs.id_contacto_sucursal = value ?? 0; }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Debe seleccionar una sucursal")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int IdSucursal
        {
            get { return _cs.id_sucursal_cliente; }
            set { _cs.id_sucursal_cliente = value; }
        }
        [StringLength(100)]
        [Required(ErrorMessage = "El campo nombre no puede estar vacío.")]
        public string Nombre
        {
            get { return _cs.nombre; }
            set { _cs.nombre = value; }
        }
        [StringLength(30)]
        [Required(ErrorMessage = "El campo apellido no puede estar vacío.")]
        public string Apellido
        {
            get { return _cs.apellido; }
            set { _cs.apellido = value; }
        }

        /*public int? Telefono
        {
            get { return _cs.telefono; }
            set { _cs.telefono = value; }
        }
        [StringLength(100)]
        public string Mail
        {
            get { return _cs.mail; }
            set { _cs.mail = value; }
        }*/

        public string FF_Baja
        {
            get { return _cs.ff_baja.HasValue ? _cs.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _cs.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool EsBaja { get; set; }
    }

}
