﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ProductoDTO
    {
        private readonly Producto _p = new Producto();

        public ProductoDTO()
        {

        }

        public ProductoDTO(Producto producto)
        {
            _p = producto;
        }

        public Producto GetProducto() => _p;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _p.id_producto; }
            set { _p.id_producto = value ?? 0; }
        }
        [StringLength(30)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _p.descripcion; }
            set { _p.descripcion = value; }
        }

        public int IdMarcaProducto
        {
            get { return _p.id_marca_producto; }
            set { _p.id_marca_producto = value; }
        }

        public int IdModeloProducto
        {
            get { return _p.id_modelo_producto; }
            set { _p.id_modelo_producto = value; }
        }

        public decimal VolumenCm
        {
            get { return _p.volumen_cm; }
            set { _p.volumen_cm = value; }
        }

        public decimal PesoReal
        {
            get { return _p.peso_real; }
            set { _p.peso_real = value; }
        }

        public int ValorUnitario
        {
            get { return _p.valor_unitario; }
            set { _p.valor_unitario = value; }
        }

        [StringLength(300)]
        public string Observaciones
        {
            get { return _p.observaciones; }
            set { _p.observaciones = value; }
        }

        public string ff_alta
        {
            get { return _p.ff_alta.ToString("dd-MM-yyyy"); }
            set { _p.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        public string ff_baja
        {
            get { return _p.ff_baja.HasValue ? _p.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _p.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

    }
}
