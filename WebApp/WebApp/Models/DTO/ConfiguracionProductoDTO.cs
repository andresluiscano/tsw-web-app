﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ConfiguracionProductoDTO
    {
        private readonly ConfiguracionProducto _cp = new ConfiguracionProducto();
        public ConfiguracionProductoDTO()
        {

        }
        public ConfiguracionProductoDTO(ConfiguracionProducto configuracion_producto)
        {
            _cp = configuracion_producto;
        }
        public ConfiguracionProducto GetConfiguracionProducto() => _cp;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _cp.id_configuracion_producto; }
            set { _cp.id_configuracion_producto = value ?? 0; }
        }
        [StringLength(20)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _cp.descripcion; }
            set { _cp.descripcion = value; }
        }
        [StringLength(300)]
        public string Observaciones
        {
            get { return _cp.observaciones; }
            set { _cp.observaciones = value; }
        }
        public string ff_alta
        {
            get { return _cp.ff_alta.ToString("dd-MM-yyyy"); }
            set { _cp.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        public string ff_baja
        {
            get { return _cp.ff_baja.HasValue ? _cp.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _cp.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
    }
}
