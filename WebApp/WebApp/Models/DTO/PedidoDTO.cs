﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class PedidoDTO
    {
        private readonly Pedido _p = new Pedido();
        public PedidoDTO()
        {

        }

        public PedidoDTO(Pedido pedido)
        {
            _p = pedido;
        }

        public Pedido GetPedido() => _p;

        public bool EsNuevo() => Id == 0;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? Id
        {
            get { return _p.id_pedido; }
            set { _p.id_pedido = value ?? 0; }
        }
        [Range(1, long.MaxValue, ErrorMessage = "Ingrese un número de pedido.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long NroPedido
        {
            get { return _p.nro_pedido; }
            set { _p.nro_pedido = value; }
        }
        [Range(1, int.MaxValue, ErrorMessage = "Seleccione una sucursal.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int IdSucursalCliente
        {
            get { return _p.id_sucursal_cliente.HasValue ? 0 : (int)_p.id_sucursal_cliente; }
            set { _p.id_sucursal_cliente = value; }
        }
        [Range(1, int.MaxValue, ErrorMessage = "Seleccione un contacto.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int IdContactoSucursal
        {
            get { return _p.id_contacto_sucursal.HasValue ? 0 : (int)_p.id_contacto_sucursal; }
            set { _p.id_contacto_sucursal = value; }
        }

        public int TelefonoContacto
        {
            get { return _p.telefono_contacto.HasValue ? 0 : (int)_p.telefono_contacto;}
            set { _p.telefono_contacto = value; }
        }

        public int EsNeutral
        {
            get { return _p.es_neutral; }
            set { _p.es_neutral = value; }
        }
        [Range(1, long.MaxValue, ErrorMessage = "Ingrese  un número de ticket.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long NroTicket
        {
            get { return _p.nro_ticket.HasValue ? 0 : (long)_p.nro_ticket; }
            set { _p.nro_ticket = value; }
        }

        public string IdEquipo
        {
            get { return _p.id_equipo; }
            set { _p.id_equipo = value; }
        }

        [StringLength(300)]
        public string Comentarios
        {
            get { return _p.comentarios; }
            set { _p.comentarios = value; }
        }

        public int IdEstadoPedido
        {
            get { return _p.id_estado_pedido; }
            set { _p.id_estado_pedido = value; }
        }

        public string ObservacionesEstado
        {
            get { return _p.observaciones_estado; }
            set { _p.observaciones_estado = value; }
        }
        /*[Range(1, int.MaxValue, ErrorMessage = "Seleccione un producto.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int IdProducto
        {
            get { return _p.id_producto; }
            set { _p.id_producto = value; }
        }
        public int? IdMarcaProducto
        {
            get { return _p.id_marca_producto; }
            set { _p.id_marca_producto = value; }
        }
        public int? IdModeloProducto
        {
            get { return _p.id_modelo_producto; }
            set { _p.id_modelo_producto = value; }
        }
        public int? IdConfiguracionProducto
        {
            get { return _p.id_configuracion_producto; }
            set { _p.id_configuracion_producto = value; }
        }
        [Range(1, int.MaxValue, ErrorMessage = "Ingrese la cantidad.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Cantidad
        {
            get { return _p.cantidad; }
            set { _p.cantidad = value; }
        }*/

        /*[Range(1, int.MaxValue, ErrorMessage = "Ingrese el proveedor de transporte.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]*/
        public int IdProveedorTransporte
        {
            get { return _p.id_proveedor_transporte.HasValue ? 0 : (int)_p.id_proveedor_transporte; }
            set { _p.id_proveedor_transporte = value; }
        }
        public string FFIngreso
        {
            get { return _p.ff_ingreso.ToString("dd/MM/yyyy HH:mm"); }
            set { _p.ff_ingreso = DateTime.ParseExact(value, "dd/MM/yyyy HH:mm", CultureInfo.CurrentCulture); }
        }
        public string FFSLA
        {
            get { return _p.ff_sla.HasValue ? _p.ff_sla.Value.ToString("dd/MM/yyyy") : ""; }
            set { _p.ff_sla = value != null ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture) : (DateTime?)null; }
        }
        public string FFCumplimiento
        {
            get { return _p.ff_cumplimiento.HasValue ? _p.ff_cumplimiento.Value.ToString("dd/MM/yyyy") : ""; }
            set { _p.ff_cumplimiento = value != null ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture) : (DateTime?)null; }
        }
        public string FFEnvioDiferido
        {
            get { return _p.ff_envio_diferido.HasValue ? _p.ff_envio_diferido.Value.ToString("dd/MM/yyyy") : ""; }
            set { _p.ff_envio_diferido = value != null ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture) : (DateTime?)null; }
        }
        public string FFEntrega
        {
            get { return _p.ff_entrega.HasValue ? _p.ff_entrega.Value.ToString("dd/MM/yyyy") : ""; }
            set { _p.ff_entrega = value != null ? DateTime.ParseExact(value, "dd/MM/yyyy", CultureInfo.CurrentCulture) : (DateTime?)null; }
        }
        public string FFBaja
        {
            get { return _p.ff_baja.HasValue ? _p.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _p.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
    }
}
