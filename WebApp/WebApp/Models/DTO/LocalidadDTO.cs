﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class LocalidadDTO
    {
        private readonly Localidad _l = new Localidad();

        public LocalidadDTO()
        {

        }

        public LocalidadDTO(Localidad localidad)
        {
            _l = localidad;
        }

        public Localidad GetLocalidad() => _l;

        public bool EsNuevo() => Id == 0;

        public long? Id
        {
            get { return _l.id_localidad; }
            set { _l.id_localidad = value ?? 0; }
        }
        public int IdProvincia
        {
            get { return _l.id_provincia; }
            set { _l.id_provincia = value; }
        }

        public string Descripcion
        {
            get { return _l.descripcion; }
            set { _l.descripcion = value; }
        }

        public double Latitud
        {
            get { return _l.latitud; }
            set { _l.latitud = value; }
        }

        public double Longitud
        {
            get { return _l.longitud; }
            set { _l.longitud = value; }
        }
    }
}
