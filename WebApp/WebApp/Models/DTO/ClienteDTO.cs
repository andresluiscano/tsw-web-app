﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ClienteDTO
    {
        private readonly Cliente _c = new Cliente();
        public ClienteDTO()
        {

        }

        public ClienteDTO(Cliente cliente)
        {
            _c = cliente;
        }

        public Cliente GetCliente() => _c;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _c.id_cliente; }
            set { _c.id_cliente = value ?? 0; }
        }

        [StringLength(50)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _c.descripcion; }
            set { _c.descripcion = value; }
        }

        public string LogoCliente
        {
            get { return _c.logo_cliente; }
            set { _c.logo_cliente = value; }
        }

        public string ff_baja
        {
            get { return _c.ff_baja.HasValue ? _c.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _c.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool esBaja { get; set; }

    }
}
