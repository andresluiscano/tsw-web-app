﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class SLATransporteDTO
    {
        private readonly SLATransporte _sla = new SLATransporte();

        public SLATransporteDTO()
        {

        }

        public SLATransporteDTO(SLATransporte sla)
        {
            _sla = sla;
        }

        public SLATransporte GetSLATransporte() => _sla;

        public bool EsNuevo() => Id == 0;

        public long? Id
        {
            get { return _sla.id_sla_transporte; }
            set { _sla.id_sla_transporte = value ?? 0; }
        }

        public int IdProveedorTransporte
        {
            get { return _sla.id_proveedor_transporte; }
            set { _sla.id_proveedor_transporte = value; }
        }
        /*[Range(1, int.MaxValue, ErrorMessage = "El campo SLA no puede estar vacío.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int CompromisoEntrega
        {
            get { return _sla.compromiso_entrega; }
            set { _sla.compromiso_entrega = value; }
        }*/

        [StringLength(10)]
        [Required(ErrorMessage = "El campo código postal no puede estar vacío.")]
        public string CodigoPostal
        {
            get { return _sla.codigo_postal; }
            set { _sla.codigo_postal = value; }
        }

        [Range(1, int.MaxValue, ErrorMessage = "El campo escala no puede estar vacío.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Escala
        {
            get { return _sla.escala; }
            set { _sla.escala = value; }
        }

        /*public int IdProvincia { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "El campo localidad no puede estar vacío.")]
        public long IdLocalidad
        {
            get { return _sla.id_localidad; }
            set { _sla.id_localidad = value; }
        }*/

        public string FF_Baja
        {
            get { return _sla.ff_baja.HasValue ? _sla.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _sla.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool EsBaja { get; set; }

    }
}
