﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class FiltroDTO
    {
        public long? nro_pedido { get; set; }
        public long? nro_ticket { get; set; }
        public int? id_cliente { get; set; }
        public int? id_estado_pedido { get; set; }
        public int? tipo_fecha { get; set; }
        public DateTime? ff_desde { get; set; }
        public DateTime? ff_hasta { get; set; }
    }
}
