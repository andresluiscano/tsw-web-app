﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class CircuitoRadialDTO
    {
        private readonly CircuitoRadial _cr = new CircuitoRadial();

        public CircuitoRadialDTO()
        {

        }

        public CircuitoRadialDTO(CircuitoRadial cr)
        {
            _cr = cr;
        }

        public CircuitoRadial GetCircuitoRadial() => _cr;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _cr.id_circuito_radial; }
            set { _cr.id_circuito_radial = value ?? 0; }
        }
        public int IdProveedorTransporte
        {
            get { return _cr.id_proveedor_transporte; }
            set { _cr.id_proveedor_transporte = value; }
        }

        [Range(1, int.MaxValue, ErrorMessage = "El campo escala no puede estar vacío.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Escala
        {
            get { return _cr.escala; }
            set { _cr.escala = value; }
        }

        [Range(1d, (double)decimal.MaxValue, ErrorMessage = "El campo valor no puede estar vacío.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal Valor
        {
            get { return _cr.valor; }
            set { _cr.valor = value; }
        }

        [Range(1d, (double)decimal.MaxValue, ErrorMessage = "El campo valor adicional no puede estar vacío.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal ValorAdicional
        {
            get { return _cr.valor_adicional; }
            set { _cr.valor_adicional = value; }
        }

        public string FF_Baja
        {
            get { return _cr.ff_baja.HasValue ? _cr.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _cr.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool EsBaja { get; set; }
    }
}
