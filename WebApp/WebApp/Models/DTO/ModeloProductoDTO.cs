﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ModeloProductoDTO
    {
        private readonly ModeloProducto _mp = new ModeloProducto();
        public ModeloProductoDTO()
        {

        }
        public ModeloProductoDTO(ModeloProducto modelo_mproducto)
        {
            _mp = modelo_mproducto;
        }
        public ModeloProducto GetModeloProducto() => _mp;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _mp.id_modelo_producto; }
            set { _mp.id_modelo_producto = value ?? 0; }
        }

        [StringLength(20)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _mp.descripcion; }
            set { _mp.descripcion = value; }
        }
        [StringLength(300)]
        public string Observaciones
        {
            get { return _mp.observaciones; }
            set { _mp.observaciones = value; }
        }
        public string ff_alta
        {
            get { return _mp.ff_alta.ToString("dd-MM-yyyy"); }
            set { _mp.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        public string ff_baja
        {
            get { return _mp.ff_baja.HasValue ? _mp.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _mp.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
    }
}
