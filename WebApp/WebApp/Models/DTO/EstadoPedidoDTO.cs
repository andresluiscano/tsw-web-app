﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class EstadoPedidoDTO
    {
        private readonly EstadoPedido _e = new EstadoPedido();

        public EstadoPedidoDTO() { 
        
        }

        public EstadoPedidoDTO(EstadoPedido estado) {
            _e = estado;
        }

        public EstadoPedido GetEstadoPedido() => _e;

        public bool EsNuevo() => Id == 0;

        public int? Id {
            get { return _e.id_estado_pedido; }
            set { _e.id_estado_pedido = value ?? 0; }
        }

        [StringLength(20)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _e.descripcion; }
            set { _e.descripcion = value; }
        }
        [StringLength(300)]
        public string Observaciones
        {
            get { return _e.observaciones; }
            set { _e.observaciones = value; }
        }
        public string ff_alta
        {
            get { return _e.ff_alta.ToString("dd-MM-yyyy"); }
            set { _e.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        public string ff_baja
        {
            get { return _e.ff_baja.HasValue ? _e.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _e.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
    }
}
