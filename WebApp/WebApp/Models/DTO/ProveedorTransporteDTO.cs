﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class ProveedorTransporteDTO
    {
        private readonly ProveedorTransporte _pt = new ProveedorTransporte();
        public ProveedorTransporteDTO()
        {

        }

        public ProveedorTransporteDTO(ProveedorTransporte prov)
        {
            _pt = prov;
        }

        public ProveedorTransporte GetProveedorTransporte() => _pt;

        public bool EsNuevo() => Id == 0;

        public int? Id
        {
            get { return _pt.id_proveedor_transporte; }
            set { _pt.id_proveedor_transporte = value ?? 0; }
        }

        [StringLength(50)]
        [Required(ErrorMessage = "El campo descripción no puede estar vacío.")]
        public string Descripcion
        {
            get { return _pt.descripcion; }
            set { _pt.descripcion = value; }
        }

        public string LogoProveedor
        {
            get { return _pt.logo_proveedor; }
            set { _pt.logo_proveedor = value; }
        }

        public decimal ValorMaximoPermitido
        {
            get { return _pt.valor_maximo_permitido; }
            set { _pt.valor_maximo_permitido = value; }
        }

        public decimal ValorPorRemito
        {
            get { return _pt.valor_por_remito; }
            set { _pt.valor_por_remito = value; }
        }

        public string ff_alta
        {
            get { return _pt.ff_alta.ToString("dd-MM-yyyy"); }
            set { _pt.ff_alta = DateTime.ParseExact(String.IsNullOrEmpty(value) && EsNuevo() ? DateTime.Today.ToString("dd-MM-yyyy") : value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }
        public string ff_baja
        {
            get { return _pt.ff_baja.HasValue ? _pt.ff_baja.Value.ToString("dd-MM-yyyy") : null; }
            set { _pt.ff_baja = DateTime.ParseExact(value, "dd-MM-yyyy", CultureInfo.CurrentCulture); }
        }

        public bool esBaja { get; set; }
    }
}
