﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DTO
{
    public class PedidoProductoDTO
    {
        private readonly PedidoProducto _pp = new PedidoProducto();
        public PedidoProductoDTO()
        {

        }

        public PedidoProductoDTO(PedidoProducto pedidoproducto)
        {
            _pp = pedidoproducto;
        }

        public PedidoProducto GetPedidoProducto() => _pp;

        public bool EsNuevo() => Id == 0;

        public long? Id
        {
            get { return _pp.id_pedido_producto; }
            set { _pp.id_pedido_producto = value ?? 0; }
        }

        public long? IdPedido
        {
            get { return _pp.id_pedido; }
            set { _pp.id_pedido = value ?? 0; }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Seleccione un producto.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int IdProducto
        {
            get { return _pp.id_producto; }
            set { _pp.id_producto = value; }
        }
        /*public int? IdMarcaProducto
        {
            get { return _pp.id_marca_producto; }
            set { _pp.id_marca_producto = value; }
        }
        public int? IdModeloProducto
        {
            get { return _pp.id_modelo_producto; }
            set { _pp.id_modelo_producto = value; }
        }*/
        public int? IdConfiguracionProducto
        {
            get { return _pp.id_configuracion_producto; }
            set { _pp.id_configuracion_producto = value; }
        }
        [Range(1, int.MaxValue, ErrorMessage = "Ingrese la cantidad.")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Cantidad
        {
            get { return _pp.cantidad; }
            set { _pp.cantidad = value; }
        }
        public int CantidadRetorno
        {
            get { return _pp.cantidad_retorno; }
            set { _pp.cantidad_retorno = value; }
        }
        /*public int CantidadPurga
        {
            get { return _pp.cantidad_purga; }
            set { _pp.cantidad_purga = value; }
        }*/
    }
}
