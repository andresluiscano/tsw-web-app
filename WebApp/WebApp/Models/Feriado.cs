﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Feriado
    {
        [Key]
        public int id_feriado { get; set; }
        public DateTime ff_feriado { get; set; }
        public string descripcion { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
