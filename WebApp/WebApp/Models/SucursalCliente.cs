﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class SucursalCliente
    {
        [Key]
        public int id_sucursal_cliente { get; set; }
        public int id_cliente { get; set; }
        public string descripcion { get; set; }
        public long id_localidad { get; set; }
        public string direccion { get; set; }
        public string codigo_postal { get; set; }     
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }


    public class SucursalClienteInterno : SucursalCliente
    {
        public int id_provincia { get; set; }
        public int? sla { get; set; }
        public long? id_codigo_postal_sucursla { get; set; }
    }

}
