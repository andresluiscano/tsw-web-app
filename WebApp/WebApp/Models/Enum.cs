﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Enum
    {
        public enum EnumEstadoPedido {
            [Description("Suspendido")] Suspendido = 1,
            [Description("Ingresado")] Ingresado = 2,
            [Description("Cancelado")] Cancelado = 3,
            [Description("Preparado")] Preparado = 4,
            [Description("Tránsito")] Transito = 5,
            [Description("Entregado")] Entregado = 6,
            [Description("Rechazado")] Rechazado = 7,
            [Description("Extraviado")] Extraviado = 8,
        }
    }
}
