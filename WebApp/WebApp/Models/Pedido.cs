﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Pedido
    {
        [Key]
        public long id_pedido { get; set; }
        public long nro_pedido { get; set; } 
        public int? id_sucursal_cliente { get; set; }
        public int? id_contacto_sucursal { get; set; }
        public int? telefono_contacto { get; set; }
        public long? nro_ticket { get; set; }
        public int es_neutral { get; set; }
        public string id_equipo { get; set; }
        public string comentarios { get; set; }
        public int id_estado_pedido { get; set; }
        public string observaciones_estado { get; set; }
        public int? id_proveedor_transporte { get; set; }
        public DateTime ff_ingreso { get; set; }
        public DateTime? ff_sla { get; set; }
        public DateTime? ff_cumplimiento { get; set; }
        public DateTime? ff_envio_diferido { get; set; }
        public DateTime? ff_entrega { get; set; }
        public DateTime ff_alta { get; set; }
        public DateTime? ff_baja { get; set; }
    }
}
