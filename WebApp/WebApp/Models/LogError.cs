﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class LogError
    {
        [Key]
        public int id_log_error { get; set; }
        public string app { get; set; }
        public DateTime ff_log_error { get; set; }
        public string nivel { get; set; }
        public string mensaje { get; set; }
        public string logger { get; set; }
        public string callsite { get; set; }
        public string excepcion { get; set; }
        public string sFF_Log_Error { get { return ff_log_error.ToString("dd/MM/yyyy HH:mm"); } }
    }
}
