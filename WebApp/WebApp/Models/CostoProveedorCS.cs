﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class CostoProveedorCS
    {
        public decimal PesoVolumetricoTotal { get; set; }
        public decimal PesoRealTotal { get; set; }
        public decimal CalculoRadial { get; set; }
        public decimal MontoPorValorDeclaradoTotal { get; set; }
        public decimal ValorPorRemito { get; set; }
        public decimal MontoTotal { get; set; }
    }
}
