#pragma checksum "C:\TSW\tsw-web-app\WebApp\WebApp\Views\EstadoPedido\EstadoPedidoView.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a056bc5d4aeebb9e778c7562e080b19befa3ccee"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_EstadoPedido_EstadoPedidoView), @"mvc.1.0.view", @"/Views/EstadoPedido/EstadoPedidoView.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/EstadoPedido/EstadoPedidoView.cshtml", typeof(AspNetCore.Views_EstadoPedido_EstadoPedidoView))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\TSW\tsw-web-app\WebApp\WebApp\Views\_ViewImports.cshtml"
using WebApp;

#line default
#line hidden
#line 2 "C:\TSW\tsw-web-app\WebApp\WebApp\Views\_ViewImports.cshtml"
using WebApp.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a056bc5d4aeebb9e778c7562e080b19befa3ccee", @"/Views/EstadoPedido/EstadoPedidoView.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fc48f17eb9bac3476d8060730298bf398eb2fa5e", @"/Views/_ViewImports.cshtml")]
    public class Views_EstadoPedido_EstadoPedidoView : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\TSW\tsw-web-app\WebApp\WebApp\Views\EstadoPedido\EstadoPedidoView.cshtml"
  
    Layout = "~/Views/Shared/_Layout.cshtml";
    ViewBag.Title = "EstadoPedido";

#line default
#line hidden
            DefineSection("Scripts", async() => {
                BeginContext(108, 13, true);
                WriteLiteral("\r\n    <script");
                EndContext();
                BeginWriteAttribute("src", " src=\"", 121, "\"", 235, 1);
#line 6 "C:\TSW\tsw-web-app\WebApp\WebApp\Views\EstadoPedido\EstadoPedidoView.cshtml"
WriteAttributeValue("", 127, Url.Content("~/assets/scripts/EstadoPedido/EstadoPedidoJS.js?v" + @DateTime.Now.ToString("yyyyMMddHHmmss")), 127, 108, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(236, 35, true);
                WriteLiteral(" type=\"text/javascript\"></script>\r\n");
                EndContext();
            }
            );
            BeginContext(274, 3167, true);
            WriteLiteral(@"
<input type=""hidden"" id=""selectedPedidoId"" name=""id_estado_pedido"" value="""" />
<div class=""row"">
    <div class=""col-md-12"">
        <!-- BEGIN Portlet PORTLET-->
        <div class=""portlet light"">
            <div class=""portlet-title"">
                <div class=""caption"">
                    <i class=""icon-user font-blue-ebonyclay""></i>
                    <span class=""caption-subject bold font-blue-ebonyclay uppercase""> Estado Pedido </span>
                </div>
            </div>
            <div class=""portlet-body"">
                <div class=""row"">
                    <div class=""col-md-8"" id=""panelTablaEstado"">
                        <!-- BEGIN TABLE PORTLET-->
                                <div class=""table table-borderless"">
                                    <table class=""table table-bordered table-striped table-condensed table-light"" id=""tabla"">
                                        <thead>
                                            <tr>
                            ");
            WriteLiteral(@"                    <th>
                                                    Descripción
                                                </th>
                                                <th>
                                                    Observaciones
                                                </th>
                                                <th>

                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            
                        
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                    <div class=""col-md-4"">
                        <!-- BEGIN ABM PORTLET-->
                        <div class=""portlet light"" id=""panelDatosCodigoPostalSLA"">
                         ");
            WriteLiteral(@"   <div class=""portlet-title"">
                                <div class=""caption font-blue-ebonyclay"">
                                    <i class=""icon-pin font-blue-ebonyclay""></i>
                                    <span class=""caption-subject bold uppercase""> GESTIONAR </span>
                                    <span class=""caption-helper"">
                                        <strong id=""tit""></strong>
                                    </span>
                                </div>
                                <div class=""actions"">
                                    <div class=""btn-group"">
                                        <a id=""nuevoEstadoPedido"" class=""btn blue-ebonyclay btn-sm"" href=""javascript:;"" onclick=""clearForm(true);"">
                                            <i class=""fa fa-plus""></i> Nuevo Estado
                                        </a>
                                    </div>
                                </div>
                            </div>");
            WriteLiteral("\r\n                            <div class=\"portlet-body form\">\r\n                                ");
            EndContext();
            BeginContext(3441, 2405, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c594ce0636142dcad7d93e53f46ffa3", async() => {
                BeginContext(3469, 2370, true);
                WriteLiteral(@"
                                    <div class=""form-body"">
                                        <div class=""row"">
                                            <div class=""col-md-12"">
                                                <div class=""alert alert-danger display-hide"">
                                                    <button class=""close"" data-close=""alert""></button>
                                                    <span id=""errorPedido""></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=""row"">
                                            <div class=""col-md-12"">
                                                <div class=""form-group"">
                                                    <label class=""control-label"">Descripción</label>
                                                    <input class=""form-control"" name=""Desc");
                WriteLiteral(@"ripcion"" id=""txtDescripcion"" maxlength=""20""/>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class=""row"">
                                            <div class=""col-md-12"">
                                                <div class=""form-group"">
                                                    <label class=""control-label"">Observaciones</label>
                                                    <textarea class=""form-control"" name=""Observaciones"" id=""txtObservaciones"" rows=""8"" maxlength=""300""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=""form-actions noborder"" align=""center"">
               ");
                WriteLiteral(@"                         <button type=""button"" onclick=""enviarForm();"" class=""btn green-jungle"">Guardar</button>
                                        <button type=""button"" onclick=""clearForm(false);"" class=""btn grey-salsa"">Cancelar</button>
                                    </div>
                                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5846, 1196, true);
            WriteLiteral(@"
                            </div>
                        </div>
                        <!-- END ABM PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=""bootbox modal fade in"" id=""modalConfirmacion"" tabindex=""-1"" role=""dialog"">
    <div class=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <button type=""button"" class=""bootbox-close-button close"" data-dismiss=""modal"" aria-hidden=""true"">×</button>
                <h4 class=""modal-title font-blue-ebonyclay uppercase""><strong>CONFIRMACIÓN</strong></h4>
            </div>
            <div class=""modal-body"">
                <div class=""bootbox-body"">¿Estás seguro que deseas <strong>borrar</strong> este estado?</div>
            </div>
            <div class=""modal-footer"">
                <button data-bb-handler=""success"" type=""button"" id=""confirmacionAceptar"" class=""btn green-jungle"">Aceptar</button>
      ");
            WriteLiteral("          <button data-bb-handler=\"danger\" type=\"button\" id=\"confirmacionCancelar\" class=\"btn red\">Cancelar</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
