﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaEstado");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/EstadoStock/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "width": "20%", "searchable": true },
            { "data": "observaciones", "name": "observaciones", "autoWidth": true, "searchable": true },
            {
                "data": "id_estado_stock", "name": "id_estado_stock", "width": "15%", "searchable": false,
                "render": function (id_estado_stock) {
                    return '<button class="btn green btn-sm" onclick="editarE(' + id_estado_stock + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarE(' + id_estado_stock + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ estados por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron estados de stock.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaEstado");
        }
    });
}
LoadData();
//-------------------------------------------------------------------------------------------
var id_estado_stock = 0;
var ff_alta;
var errorStock = $('.alert-danger', form);
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#panelDatosEstadoStock");
    $("#form").validate().resetForm();
    errorStock.hide();
    id_estado_stock = 0;
    ff_alta = null;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Observaciones"]').val("").removeClass("edited");
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#panelDatosEstadoStock :input').attr('disabled', false);
    else
        $('#panelDatosEstadoStock :input').attr('disabled', true);

    $.tsw.hideLoading("#panelDatosEstadoStock");
}
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarE = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosPedido");
    $.ajax({
        type: "GET",
        url: "/EstadoStock/GetEstadoStock",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var e = data.estadoStock;
            id_estado_stock = e.id;
            ff_alta = e.ff_alta;    
            $('[name="id_estado_stock"]').val(id_estado_stock);
            $('[name="Descripcion"]').val(e.descripcion).addClass(e.descripcion == null ? "" : "edited");
            $('[name="Observaciones"]').val(e.observaciones).addClass(e.observaciones == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#panelDatosEstadoStock");
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosEstadoStock");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_estado_stock;
        param.Ff_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/EstadoStock/EditEstadoStock",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    $('#tabla').dataTable()._fnReDraw();
                    clearForm(false);
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosEstadoStock");
            }
        });
    }
};
//-------------------------------------------------------------------------------------------
var eliminarE = function (id) {
    $('#modalConfirmacion').modal('show');
    id_estado_stock = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/EstadoStock/EliminarEstadoStock",
        data: { id: id_estado_stock },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm(false);
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});