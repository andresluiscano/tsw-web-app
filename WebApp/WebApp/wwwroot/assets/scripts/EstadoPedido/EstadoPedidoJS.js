﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaEstado");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/EstadoPedido/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "width": "20%", "searchable": true },
            { "data": "observaciones", "name": "observaciones", "autoWidth": true, "searchable": true },
            {
                "data": "id_estado_pedido", "name": "id_estado_pedido", "width": "15%", "searchable": false,
                "render": function (id_estado_pedido) {
                    return '<button class="btn green btn-sm" onclick="editarE(' + id_estado_pedido + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarE(' + id_estado_pedido + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ estados por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron estados de pedidos.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaEstado");
        }
    });
}

LoadData();
//-------------------------------------------------------------------------------------------
var id_estado_pedido = 0;
var ff_alta;
var errorPedido = $('.alert-danger', form);
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#panelDatosEstadoPedido");
    $("#form").validate().resetForm();
    errorPedido.hide();
    id_estado_pedido = 0;
    ff_alta = null;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Observaciones"]').val("").removeClass("edited");
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#panelDatosEstadoPedido :input').attr('disabled', false);
    else
        $('#panelDatosEstadoPedido :input').attr('disabled', true);

    $.tsw.hideLoading("#panelDatosEstadoPedido");
}
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarE = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosPedido");
    $.ajax({
        type: "GET",
        url: "/EstadoPedido/GetEstadoPedido",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var e = data.estadoPedido;
            id_estado_pedido = e.id;
            ff_alta = e.ff_alta;
            $('[name="id_estado_pedido"]').val(id_estado_pedido);
            $('[name="Descripcion"]').val(e.descripcion).addClass(e.descripcion == null ? "" : "edited");
            $('[name="Observaciones"]').val(e.observaciones).addClass(e.observaciones == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#panelDatosEstadoPedido");
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#panelDatosEstadoPedido");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_estado_pedido;
        param.Ff_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/EstadoPedido/EditEstadoPedido",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    $('#tabla').dataTable()._fnReDraw();
                    clearForm(false);
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#panelDatosEstadoPedido");
            }
        });
    }
};
//-------------------------------------------------------------------------------------------
var eliminarE = function (id) {
    $('#modalConfirmacion').modal('show');
    id_estado_pedido = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/EstadoPedido/EliminarEstadoPedido",
        data: { id: id_estado_pedido },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 2) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm(false);
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            }
            else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            }
            else {
                $.tsw.showMessageError(data.mensaje);
            }
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});