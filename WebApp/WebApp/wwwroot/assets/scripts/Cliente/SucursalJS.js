﻿//-------------------------------------------------------------------------------------------
$.tsw.llenarCombos("cmbProvincia", "/Provincia/GetProvincias", "", "0");
//-------------------------------------------------------------------------------------------
var id_provincia = 0;
var id_sucursal_cliente = 0;
var cpsla = null;
//-------------------------------------------------------------------------------------------
var LoadSucursales = function (id) {
    $.tsw.showLoading("Espere", "#PanelTablaSucursales");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Cliente/LoadSucursales",
            "type": "POST",
            "data": { id_cliente: id },
            "datatype": "json"
        },
        "columns": [
            {
                "data": "descripcion", "name": "descripcion", "autoWidth": true, "searchable": true,
                "render": function (data, type, full) {
                    return full.descripcion;
                }
            },
            {
                "data": "direccion", "name": "direccion", "autoWidth": true, "searchable": true,
                "render": function (data, type, full) {
                    return full.direccion;
                }
            },
            {
                "data": "id_sucursal_cliente", "name": "id_sucursal_cliente", "autoWidth": true, "searchable": false,
                "render": function (id_sucursal_cliente) {
                    return '<button class="btn green btn-sm" onclick="editarSucursal(' + id_sucursal_cliente + ');"><i class="fa fa-edit"></i> </button></button> <button class="btn red btn-sm" onclick="eliminarSucursal(' + id_sucursal_cliente + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ sucursales por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron sucursales para el cliente seleccionado.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#PanelTablaSucursales");
        }
    });
}
//-------------------------------------------------------------------------------------------
$('#cmbProvincia').on('change', function () {
    if ($(this).val() != 0 && $(this).val() != null && $(this).val() != undefined)
        $.tsw.llenarCombos("cmbLocalidad", "/Localidad/GetLocalidades?id_provincia=" + $(this).val(), "", "0");
});
//-------------------------------------------------------------------------------------------
var clearFormSucursal = function () {
    //$("#form").validate().resetForm();
    //errorProvincia.hide();
    $('#modalSucursal').modal('hide'); 
    id_sucursal_cliente = 0;
    $('[name="DescripcionSucursal"]').val("").removeClass("edited");
    $('[name="IdLocalidad"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdProvincia"]').val("").removeClass("edited").trigger('change');
    $('[name="Direccion"]').val("").removeClass("edited");
    $('[name="CodigoPostal"]').val("").removeClass("edited");
    $('[name="SLA"]').val("").removeClass("edited");
    window.scrollTo(0, 0);
    cpsla = null;
}
//-------------------------------------------------------------------------------------------
var enviarFormSucursal = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosSucursal");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_sucursal_cliente;
        param.IdCliente = id_cliente;

        $.ajax({
            type: "POST",
            url: "/Cliente/EditSucursal",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearFormSucursal();
                    $('#tabla').dataTable()._fnReDraw();
                    //Hay que recargar el combo de sucursales en el contacto
                    $.tsw.llenarCombos("cmbSucursal", "/Cliente/GetSucursales?id_cliente=" + id_cliente, "", "0");
                } else if (data.code === 4) {
                    $.tsw.showMessageWarning(data.mensaje);
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosSucursal");
            }
        });
    }
}
//-------------------------------------------------------------------------------------------
var editarSucursal = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosSucursal");
    $.ajax({
        type: "GET",
        url: "/Cliente/GetSucursalCliente",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearFormSucursal();
            var s = data.sucursal;
            cpsla = data.codigoPostalSLA;

            id_sucursal_cliente = s.id;
            $('[name="DescripcionSucursal"]').val(s.descripcionSucursal).addClass(s.descripcionSucursal == null ? "" : "edited");
            $('[name="IdProvincia"]').val(s.idProvincia).removeClass("edited").trigger('change');
            $.tsw.llenarCombos("cmbLocalidad", "/Localidad/GetLocalidades?id_provincia=" + s.idProvincia, "", s.idLocalidad, "", "0");
            $('[name="Direccion"]').val(s.direccion).addClass(s.direccion == null ? "" : "edited");
            $('[name="CodigoPostal"]').val(s.codigoPostal).addClass(s.codigoPostal == null ? "" : "edited");
            $('[name="SLA"]').val(s.sla).addClass(s.sla == null ? "" : "edited");
            if (cpsla != null) {
                $('[name="SLA"]').val(cpsla.sla).addClass(cpsla.sla == null ? "" : "edited");
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $('#modalSucursal').modal('show');
            $.tsw.hideLoading("#PanelDatosSucursal");
        }
    });
};
//-------------------------------------------------------------------------------------------
$('#modalSucursal').on('hidden.bs.modal', function () {
    clearFormSucursal();
});
//------------------------------------------------------------------------------------------
var eliminarSucursal = function (id) {
    $('#modalConfirmacionSucursal').modal('show');
    id_sucursal_cliente = id;
};
$("#confirmacionAceptarSucursal").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Cliente/EliminarSucursal",
        data: { id: id_sucursal_cliente },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                $('#tabla').dataTable()._fnReDraw();
            } else
                $.tsw.showMessageError(data.mensaje);           
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $('#modalConfirmacionSucursal').modal('hide');
            clearFormSucursal();
        }
    });
});
$("#confirmacionCancelarSucursal").on("click", function (e) {
    $('#modalConfirmacionSucursal').modal('hide');
    clearFormSucursal();
});
//------------------------------------------------------------------------------------------
var UpdateSLA = function () {
    $.tsw.showLoading("Espere", "#PanelDatosSucursal");
    var param = {};
    if (cpsla != null) {
        param = cpsla;
        param.sla = parseInt($('[name="SLA"]').val());
        param.ffBaja = null;
    } else {
        param.Id = 0;
        param.Sla = $('[name="SLA"]').val();
        param.CodigoPostal = $('[name="CodigoPostal"]').val();
    }
    $.ajax({
        type: "POST",
        url: "/CodigoPostalSLA/EditCodigoPostalSLA",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormSucursal();
            } else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosSucursal");
        }
    });
};