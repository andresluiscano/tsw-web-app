﻿var id_cliente;
var FF_Alta;
var logo_cliente;
//-------------------------------------------------------------------------------------------
var initialize = function () {
    id_cliente = 0;
    ff_alta = null;
    logo_cliente = null;
    $("#btnGuardar").addClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").addClass("hidden");
    $("#btnEditar").addClass("hidden");
    $("#btnNuevo").removeClass("hidden");
    $('#descripcionNoEditable').text("");
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('#DivDescripcionNoEditable').show();
    $('#DivDescripcionEditable').hide();
    
    $('#logo_cliente').attr('src', '/assets/img/profile_user.jpg');
    $("#inputFile").attr("disabled", true);
    $("avisoBusqueda").hide();
    $("#avisoImagen").hide();
    $("#nuevoContacto").attr('disabled', 'disabled');
    $("#nuevaSucursal").attr('disabled', 'disabled');
    //$.tsw.llenarCombos("cmbCliente", "Cliente/GetClientes", "", "0");
    //$("#inputCliente").val("");
}
initialize();
//-------------------------------------------------------------------------------------------
var enviarForm = function (esBaja) {
    var param = {
        Id: id_cliente,
        Descripcion: $("#descripcion").val(),
        ff_alta: ff_alta,
        esBaja: esBaja,
        LogoCliente: logo_cliente
    };

    $.ajax({
        type: "POST",
        url: "/Cliente/EditCliente",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {//Edito OK
                $.tsw.showMessageOk(data.mensaje);
                buscarCliente(param.Descripcion);
            }
            else if (data.code === 2) {//Borra lógicamente OK
                $.tsw.showMessageOk(data.mensaje);
                $("avisoBusqueda").show();
                initialize();
            } else if (data.code === 4) {//No se paso alguna validación
                $.tsw.showMessageWarning(data.mensaje);
            } else { //Ocurrió una excepción
                $.tsw.showMessageError(data.mensaje);
            }
        }
    });
}
//-------------------------------------------------------------------------------------------
var buscarCliente = function (id) {
    $.ajax({
        type: "GET",
        url: "/Cliente/GetCliente",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code == 1) { //Existe el cliente
                $("#btnGuardar").addClass("hidden");
                $("#btnCancelar").addClass("hidden");
                $("#btnEditar").removeClass("hidden");
                $("#btnBorrar").removeClass("hidden");
                $("#btnBorrar").text("Borrar");
                $("#btnBorrar").removeClass("grey-salsa eliminar").addClass("red");
                $("#btnNuevo").addClass("hidden");
                $("#btnBorrar").addClass("eliminar");
                $("#avisoBusqueda").hide();

                var p = data.cliente;
                id_cliente = p.id;
                logo_cliente = p.logoCliente != null ? p.logoCliente : '/assets/img/profile_user.jpg';;
                ff_alta = p.ff_alta;
                $('[name="id_cliente"]').val(id_cliente);
                $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
                $('#descripcionNoEditable').text(p.descripcion);
                $('#DivDescripcionNoEditable').show();
                $('#DivDescripcionEditable').hide();
                $('#logo_cliente').attr('src', logo_cliente);
                //Sucursales
                clearFormSucursal();
                if (!$.fn.DataTable.isDataTable('#tabla')) {
                    LoadSucursales(p.id);
                }
                else {
                    $('#tabla').dataTable().fnClearTable();
                    LoadSucursales(p.id);
                }
                //Contactos
                clearFormContacto();
                $.tsw.llenarCombos("cmbSucursal", "/Cliente/GetSucursales?id_cliente=" + id_cliente, "", "0");
                
                if (!$.fn.DataTable.isDataTable('#tablaContactos')) {
                    LoadContactos(p.id);
                }
                else {
                    $('#tablaContactos').dataTable().fnClearTable();
                    LoadContactos(p.id);
                }

                $("#nuevoContacto").removeAttr('disabled');
                $("#nuevaSucursal").removeAttr('disabled');
            } else {//No se encontró el cliente
                $("#avisoBusqueda").show();
                initialize();
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
}
//-------------------------------------------------------------------------------------------
$("#btnNuevo").on("click", function (e) {
    $("#btnEditar").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $(this).addClass("hidden");
    $('#DivDescripcionEditable').show();
    $('#DivDescripcionNoEditable').hide();
    $("#btnBorrar").addClass("eliminar");
    $("#avisoBusqueda").hide();
    $("#avisoImagen").show();
    $("#cmbCliente").val(0);
    $("#inputFile").attr("disabled", false);
});
//-------------------------------------------------------------------------------------------
$("#btnEditar").on("click", function (e) {
    $("#btnNuevo").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $(this).addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $('#DivDescripcionEditable').show();
    $('#DivDescripcionNoEditable').hide();
    $("#inputFile").attr("disabled", false);
    $("#avisoImagen").show();
});
$("#cmbCliente").on("change", function () {
    if ($(this).val() != 0 && $(this).val() != null)
        buscarCliente(parseInt($("#cmbCliente").val()));
});
$(".eliminar").on("click", function (e) {
    //enviarForm(true);
    //initialize();
    $('#modalConfirmacionCliente').modal('show');
});
$(".cancelar").on("click", function (e) {
    if (id_cliente != 0) {
        $("#btnGuardar").addClass("hidden");
        $("#btnBorrar").removeClass("hidden");
        $("#btnCancelar").addClass("hidden");
        $("#btnEditar").removeClass("hidden");
        $("#btnNuevo").addClass("hidden");
    } else {
        $("#btnNuevo").removeClass("hidden");
        $("#btnGuardar").addClass("hidden");
        $("#btnBorrar").addClass("hidden");
        $("#btnCancelar").addClass("hidden");
        $("#btnEditar").addClass("hidden");
        $('#logo_cliente').attr('src', '/assets/img/profile_user.jpg');
    }
    $("#inputFile").attr("disabled", true);
    $('#DivDescripcionEditable').hide();
    $('#DivDescripcionNoEditable').show();
    $("#avisoImagen").hide();
});
$("#logo_cliente").on("click", function (e) {
    $("#inputFile").click();
})
//-------------------------------------------------------------------------------------------
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#logo_cliente').attr('src', e.target.result);
            logo_cliente = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#inputFile").change(function () {
    readURL(this);
});
//------------------------------------------------------------------------------------------
$("#confirmacionAceptarCliente").on("click", function (e) {
    enviarForm(true);
    $('#modalConfirmacionCliente').modal('hide');
    //initialize();
});
$("#confirmacionCancelarCliente").on("click", function (e) {
    $('#modalConfirmacionCliente').modal('hide');
});