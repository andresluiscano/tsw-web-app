﻿//-------------------------------------------------------------------------------------------
var id_contacto_sucursal = 0;
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
};
//-------------------------------------------------------------------------------------------
var enviarFormContacto = function () {
    if ($("#formContacto").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosContacto");
        var param = objectifyForm($("#formContacto").serializeArray());
        param.Id = id_contacto_sucursal;

        $.ajax({
            type: "POST",
            url: "/Cliente/EditContacto",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearFormContacto();
                    $('#tablaContactos').dataTable()._fnReDraw();
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosContacto");
            }
        });
    }
};
//-------------------------------------------------------------------------------------------
var clearFormContacto = function () {
    //$("#form").validate().resetForm();
    //errorProvincia.hide();
    $('#modalContacto').modal('hide');
    id_contacto_sucursal = 0;
    $('[name="Nombre"]').val("").removeClass("edited");
    $('[name="Apellido"]').val("").removeClass("edited");
    $('[name="IdSucursal"]').val("").removeClass("edited").trigger('change');
    /*$('[name="Telefono"]').val("").removeClass("edited");
    $('[name="Mail"]').val("").removeClass("edited");*/
    window.scrollTo(0, 0);
};
//-------------------------------------------------------------------------------------------
var LoadContactos = function (id) {
    $.tsw.showLoading("Espere", "#PanelTablaContactos");
    $("#tablaContactos").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Cliente/LoadContactos",
            "type": "POST",
            "data": { id_cliente: id },
            "datatype": "json"
        },
        "columns": [
            {
                "data": "nombre", "name": "nombre", "autoWidth": true, "searchable": true,
                "render": function (data, type, full) {
                    return full.nombre + " " + full.apellido;
                }
            },
            {
                "data": "sucursal", "name": "sucursal", "autoWidth": true, "searchable": true, "searchable": true
            },
            {
                "data": "id_contacto_sucursal", "name": "id_contacto_sucursal", "autoWidth": true, "searchable": false,
                "render": function (id_contacto_sucursal) {
                    return '<button class="btn green btn-sm" onclick="editarContacto(' + id_contacto_sucursal + ');"><i class="fa fa-edit"></i> </button> </button> <button class="btn red btn-sm" onclick="eliminarContacto(' + id_contacto_sucursal + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ contactos por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron contactos para el cliente seleccionado.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#PanelTablaContactos");
        }
    }); 
};
//-------------------------------------------------------------------------------------------
var editarContacto = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosContacto");
    $.ajax({
        type: "GET",
        url: "/Cliente/GetContactoSucursal",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearFormSucursal();
            var cs = data.contacto;
            id_contacto_sucursal = cs.id;
            $('[name="Nombre"]').val(cs.nombre).addClass(cs.nombre == null ? "" : "edited");
            $('[name="Apellido"]').val(cs.apellido).addClass(cs.apellido == null ? "" : "edited");
            $('[name="IdSucursal"]').val(cs.idSucursal).removeClass("edited").trigger('change');
            /*$('[name="Telefono"]').val(cs.telefono).addClass(cs.telefono == null ? "" : "edited");
            $('[name="Mail"]').val(cs.mail).addClass(cs.mail == null ? "" : "edited");*/
            $('#modalContacto').modal('show');
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosContacto");

        }
    });
};
//-------------------------------------------------------------------------------------------
$('#modalContacto').on('hidden.bs.modal', function () {
    clearFormSucursal();
});
//-------------------------------------------------------------------------------------------
var eliminarContacto = function (id) {
    $('#modalConfirmacionContacto').modal('show');
    id_sucursal_cliente = id;
};
$("#confirmacionAceptarContacto").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Cliente/EliminarContacto",
        data: { id: id_sucursal_cliente },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 2) {
                $.tsw.showMessageOk(data.mensaje);
                $('#modalConfirmacionContacto').modal('hide'); 
                clearFormSucursal();
                $('#tablaContactos').dataTable()._fnReDraw();
            }
            else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            } else 
                $.tsw.showMessageError(data.mensaje);   
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
});
$("#confirmacionCancelarContacto").on("click", function (e) {
    $('#modalConfirmacionContacto').modal('hide');
    clearFormContacto();
});