﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaProducto");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Producto/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "searchable": true, "width": "auto" },
            { "data": "marca_descripcion", "name": "marca_descripcion", "searchable": true, "width": "auto" },
            { "data": "modelo_descripcion", "name": "modelo_descripcion", "searchable": true, "width": "auto" },
            { "data": "volumen_cm", "name": "volumen_cm", "searchable": true, "width": "auto" },
            { "data": "peso_real", "name": "peso_real", "searchable": true, "width": "auto" },
            { "data": "valor_unitario", "name": "valor_unitario", "searchable": true, "width": "auto" },
            { "data": "observaciones", "name": "observaciones", "searchable": true, "width": "auto" },
            {
                "data": "id_producto", "name": "id_producto", "searchable": false, "className": "dt-center", "width": "auto",
                "render": function (id_producto) {
                    return '<button class="btn green btn-sm" onclick="editarP(' + id_producto + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarProducto(' + id_producto + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ productos por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron productos.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaProducto");
        }
    });
}
LoadData();
//-------------------------------------------------------------------------------------------
var id_producto = 0;
var ff_alta;
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#PanelDatosProducto");
    $("#form").validate().resetForm();
    //errorproducto.hide();
    id_producto = 0;
    ff_alta = null;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Observaciones"]').val("").removeClass("edited");
    $('[name="VolumenCm"]').val("").removeClass("edited");
    $('[name="PesoReal"]').val("").removeClass("edited");
    $('[name="ValorUnitario"]').val("").removeClass("edited");
    $('[name="IdMarcaProducto"]').val(0).removeClass("edited").trigger('change');
    $('[name="IdModeloProducto"]').val(0).removeClass("edited").trigger('change');
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#PanelDatosProducto :input').attr('disabled', false);
    else
        $('#PanelDatosProducto :input').attr('disabled', true);

    $.tsw.hideLoading("#PanelDatosProducto");
};
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarP = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosProducto");
    $.ajax({
        type: "GET",
        url: "/Producto/GetProducto",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var p = data.producto;
            id_producto = p.id;
            ff_alta = p.ff_alta;
            $('[name="id_producto"]').val(id_producto);
            $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
            $('[name="VolumenCm"]').val(p.volumenCm).addClass(p.volumenCm == null ? "" : "edited");
            $('[name="PesoReal"]').val(p.pesoReal).addClass(p.pesoReal == null ? "" : "edited");
            $('[name="ValorUnitario"]').val(p.valorUnitario).addClass(p.valorUnitario == null ? "" : "edited");
            $('[name="Observaciones"]').val(p.observaciones).addClass(p.observaciones == null ? "" : "edited");
            $('[name="IdMarcaProducto"]').val(p.idMarcaProducto).removeClass("edited").trigger('change');
            $('[name="IdModeloProducto"]').val(p.idModeloProducto).removeClass("edited").trigger('change');
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosProducto");
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosProducto");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_producto;
        param.Ff_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/Producto/EditProducto",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearForm();
                    $('#tabla').dataTable()._fnReDraw();
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosProducto");
                
            }
        });
    }
};
//------------------------------------------------------------------------------------------
var eliminarProducto = function (id) {
    $('#modalConfirmacion').modal('show');
    id_producto = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Producto/EliminarProducto",
        data: { id: id_producto },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm();
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});
