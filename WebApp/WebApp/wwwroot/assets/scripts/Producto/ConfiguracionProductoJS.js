﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaConfiguracion");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Producto/LoadDataConfiguracion",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "autoWidth": true, "searchable": true },
            { "data": "observaciones", "name": "observaciones", "autoWidth": true, "searchable": true },
            {
                "data": "id_configuracion_producto", "name": "id_configuracion_producto", "autoWidth": true, "searchable": false, "className": "dt-center",
                "render": function (id_configuracion_producto) {
                    return '<button class="btn green btn-sm" onclick="editarC(' + id_configuracion_producto + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarConfiguracion(' + id_configuracion_producto + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ configuraciones por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron configuraciones.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaConfiguracion");
        }
    });
}
LoadData();
//-------------------------------------------------------------------------------------------
var id_configuracion_producto = 0;
var ff_alta;
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#PanelDatosConfiguracion");
    $("#form").validate().resetForm();
    //errorproducto.hide();
    id_configuracion_producto = 0;
    ff_alta = null;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Observaciones"]').val("").removeClass("edited");
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#PanelDatosConfiguracion :input').attr('disabled', false);
    else
        $('#PanelDatosConfiguracion :input').attr('disabled', true);

    $.tsw.hideLoading("#PanelDatosConfiguracion");
};
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarC = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosConfiguracion");
    $.ajax({
        type: "GET",
        url: "/Producto/GetConfiguracion",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var p = data.configuracionProducto;
            id_configuracion_producto = p.id;
            ff_alta = p.ff_alta;          
            $('[name="id_configuracion_producto"]').val(id_configuracion_producto);
            $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
            $('[name="Observaciones"]').val(p.observaciones).addClass(p.observaciones == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {          
            $.tsw.hideLoading("#PanelDatosConfiguracion");
        }
    });
};
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosConfiguracion");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_configuracion_producto;
        param.Ff_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/Producto/EditConfiguracion",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    $('#tabla').dataTable()._fnReDraw();
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosConfiguracion");
                clearForm(false);
            }
        });
    }
};
//------------------------------------------------------------------------------------------
var eliminarConfiguracion = function (id) {
    $('#modalConfirmacion').modal('show');
    id_configuracion_producto = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Producto/EliminarConfiguracion",
        data: { id: id_configuracion_producto },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm(false);
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});
