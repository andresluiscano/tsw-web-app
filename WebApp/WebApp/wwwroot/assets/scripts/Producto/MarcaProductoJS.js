﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaMarca");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Producto/LoadDataMarca",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "autoWidth": "40%", "searchable": true },
            { "data": "observaciones", "name": "observaciones", "autoWidth": "40%", "searchable": true },
            {
                "data": "id_marca_producto", "name": "id_marca_producto", "width": "20%", "searchable": false, "className": "dt-center",
                "render": function (id_marca_producto) {
                    return '<button class="btn green btn-sm" onclick="editarP(' + id_marca_producto + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarMarca(' + id_marca_producto + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ marcas por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron marcas.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaMarca");
        }
    });
}
LoadData();
//-------------------------------------------------------------------------------------------
var id_marca_producto = 0;
var ff_alta;
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#PanelDatosMarca");
    $("#form").validate().resetForm();
    id_marca_producto = 0;
    ff_alta = null;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Observaciones"]').val("").removeClass("edited");
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#PanelDatosMarca :input').attr('disabled', false);
    else
        $('#PanelDatosMarca :input').attr('disabled', true);

    $.tsw.hideLoading("#PanelDatosMarca");
};
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarP = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosMarca");
    $.ajax({
        type: "GET",
        url: "/Producto/GetMarca",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var p = data.marcaProducto;
            id_marca_producto = p.id;
            ff_alta = p.ff_alta;
            $('[name="id_marca_producto"]').val(id_marca_producto);
            $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
            $('[name="Observaciones"]').val(p.observaciones).addClass(p.observaciones == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosMarca");
        }
    });
};
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosMarca");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_marca_producto;
        param.Ff_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/Producto/EditMarca",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    $('#tabla').dataTable()._fnReDraw();
                    clearForm(false);
                }
                else if (data.code === 4) 
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosMarca");
            }
        });
    }
};
//------------------------------------------------------------------------------------------
var eliminarMarca = function (id) {
    $('#modalConfirmacion').modal('show');
    id_marca_producto = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Producto/EliminarMarca",
        data: { id: id_marca_producto },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm(false);
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});
//------------------------------------------------------------------------------------------
