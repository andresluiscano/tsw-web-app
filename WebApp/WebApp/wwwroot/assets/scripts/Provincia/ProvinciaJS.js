﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaProvincia")
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Provincia/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "autoWidth": true, "searchable": true },
            { "data": "latitud", "name": "latitud", "autoWidth": true, "searchable": true },
            { "data": "longitud", "name": "longitud", "autoWidth": true, "searchable": true },
            {
                "data": "id_provincia", "name": "id_provincia", "autoWidth": true, "searchable": false,
                "render": function (id_provincia) {
                    return '<button class="btn green btn-sm" onclick="editarP(' + id_provincia + ');"><i class="fa fa-edit"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ provincias por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron provincias.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaProvincia");
        }
    });  
}

LoadData();
//-------------------------------------------------------------------------------------------
var id_provincia = 0;
//-------------------------------------------------------------------------------------------
var clearForm = function () {
    $("#form").validate().resetForm();
    //errorProvincia.hide();
    id_provincia = 0;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Latitud"]').val("").removeClass("edited");
    $('[name="Longitud"]').val("").removeClass("edited");
    window.scrollTo(0, 0);
}
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarP = function (id) {

    $.ajax({
        type: "GET",
        url: "/Provincia/GetProvincia",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            clearForm();
            var p = data.provincia;
            id_provincia = p.id;
            $('[name="id_provincia"]').val(id_provincia);
            $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
            $('[name="Latitud"]').val(p.latitud).addClass(p.latitud == null ? "" : "edited");
            $('[name="Longitud"]').val(p.longitud).addClass(p.longitud == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {

        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_provincia;

        $.ajax({
            type: "POST",
            url: "/Provincia/EditProvincia",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearForm();
                    $('#tabla').dataTable()._fnReDraw();
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            }
        });
    }
}