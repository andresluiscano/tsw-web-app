﻿var arr = { 'Debug': 'default', 'Info': 'info', 'Warn': 'warning', 'Error': 'danger', 'Fatal': 'primary'};

//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaLogError");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "order": [[0, "desc"]],
        "pageLength": 100,
        "search": true,
        "ajax": {
            "url": "/Error/LoadLogErrorData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            {
                "data": "sFF_Log_Error", "name": "sFF_Log_Error", "width": "15%", "searchable": true,
                "render": function (sFF_Log_Error) {
                    return "<text style='font-size: 10px;'>" + sFF_Log_Error + "<text>";
                }
            },
            {
                "data": "nivel", "name": "nivel", "width": "7%", "searchable": false,
                "render": function (nivel) {
                    return '<span style="font-size: 10px;" class="label label-sm label-' + arr[nivel] + '">' + nivel + '</span>';
                }
            },
            {
                "data": "mensaje", "name": "mensaje", "width": "20%",
                "render": function (mensaje) {
                    return "<text style='font-size: 10px;'>" + mensaje + "<text>";
                }
            },
            {
                "data": "callsite", "name": "callsite",
                "render": function (callsite) {
                    var txt = callsite.length >= 42 ? callsite.substring(0, 42) + "..." : callsite;
                    return "<text style='font-size: 10px;' title='" + callsite + "'>" + txt + "<text>";
                }
            },
            {
                "data": "excepcion", "name": "excepcion",
                "render": function (excepcion) {
                    //var txt = excepcion.length >= 50 ? excepcion.substring(0, 50) + "..." : excepcion;
                    var txt = excepcion.length == 0 ? '-' : excepcion;
                    return "<text style='font-size: 10px;' >" + $.tsw.encodeHTML(txt) + "<text>";
                }
            },
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ errores por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron log de errores.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaLogError");
        }
    });
}

LoadData();
//-------------------------------------------------------------------------------------------