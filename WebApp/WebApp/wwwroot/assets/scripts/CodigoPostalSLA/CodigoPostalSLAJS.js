﻿//-------------------------------------------------------------------------------------------
var LoadData = function () {
    $.tsw.showLoading("Espere", "#panelTablaSLA");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/CodigoPostalSLA/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "codigo_postal", "name": "codigo_postal", "width": "50%", "searchable": true },
            { "data": "sla", "name": "sla", "autoWidth": true, "searchable": true },
            {
                "data": "id_codigo_postal_sla", "name": "id_codigo_postal_sla", "width": "15%", "searchable": false,
                "render": function (id_codigo_postal_sla) {
                    return '<button class="btn green btn-sm" onclick="editarE(' + id_codigo_postal_sla + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarE(' + id_codigo_postal_sla + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ estados por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron SLA's.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#panelTablaSLA");
        }
    });
}
LoadData();
//-------------------------------------------------------------------------------------------
var id_codigo_postal_sla = 0;
var ff_alta;
var errorSLA = $('.alert-danger', form);
//-------------------------------------------------------------------------------------------
var clearForm = function (newOrEdit) {
    $.tsw.showLoading("Espere", "#panelDatosCodigoPostalSLA");
    $("#form").validate().resetForm();
    errorSLA.hide();
    id_codigo_postal_sla = 0;
    ff_alta = null;
    $('[name="CodigoPostal"]').val("").removeClass("edited");
    $('[name="SLA"]').val("").removeClass("edited");
    window.scrollTo(0, 0);

    if (newOrEdit)
        $('#panelDatosCodigoPostalSLA :input').attr('disabled', false);
    else
        $('#panelDatosCodigoPostalSLA :input').attr('disabled', true);

    $.tsw.hideLoading("#panelDatosCodigoPostalSLA");
}
clearForm(false);
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var editarE = function (id) {
    $.tsw.showLoading("Espere", "#panelDatosCodigoPostalSLA");
    $.ajax({
        type: "GET",
        url: "/CodigoPostalSLA/GetCodigoPostalSLA",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm(true);
            var e = data.codigoPostalSLA;
            console.log(e);
            id_codigo_postal_sla = e.id;
            ff_alta = e.ff_alta;
            $('[name="CodigoPostal"]').val(e.codigoPostal).addClass(e.codigoPostal == null ? "" : "edited");
            $('[name="SLA"]').val(e.sla).addClass(e.sla == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#panelDatosCodigoPostalSLA");
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#panelDatosCodigoPostalSLA");
        var param = objectifyForm($("#form").serializeArray());
        param.id = id_codigo_postal_sla;
        param.FF_alta = ff_alta;
        $.ajax({
            type: "POST",
            url: "/CodigoPostalSLA/EditCodigoPostalSLA",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    $('#tabla').dataTable()._fnReDraw();
                    clearForm(false);
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#panelDatosCodigoPostalSLA");
            }
        });
    }
};
//-------------------------------------------------------------------------------------------
var eliminarE = function (id) {
    $('#modalConfirmacion').modal('show');
    id_codigo_postal_sla = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/CodigoPostalSLA/EliminarCodigoPostalSLA",
        data: { id: id_codigo_postal_sla },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 2) {
                $.tsw.showMessageOk(data.mensaje);
                clearForm(false);
                $('#modalConfirmacion').modal('hide');
                $('#tabla').dataTable()._fnReDraw();
            }
            else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            }
            else {
                $.tsw.showMessageError(data.mensaje);
            }
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearForm();
});