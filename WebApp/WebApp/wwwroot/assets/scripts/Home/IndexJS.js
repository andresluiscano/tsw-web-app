﻿var id_cliente = 0;
var id_pedido = 0;
$('#ffHasta, #ffDesde').datetimepicker({
    format: "dd/mm/yyyy",
    defaultDate: new Date(),
    language: 'es',
    autoclose: true,
    immediateUpdates: true,
    todayBtn: true,
    todayHighlight: true,
    pickerPosition: "bottom-left",
    disabledHours: true,
    pickTime: false,
    minView: "month",
});
//-------------------------------------------------------------------------------------------
//I = estado inicial, C = estados en curso , F = estados finales
var LoadPedidos = function (estado) {
    $.tsw.showLoading("Espere", "#panelTablaPedidos")
    /*var estadoSelected = "";
    var classLabel = ""
    switch (estado) {
        case "I":
            $("#estadoDesc").text("Nuevos");
            estadoSelected = "nuevos";
            classLabel = "danger";
            break;
        case "C":
            $("#estadoDesc").text("En Curso");
            estadoSelected = "en curso";
            classLabel = "warning";
            break;
        default:
            $("#estadoDesc").text("Finalizados");
            estadoSelected = "finalizados";
            classLabel = "success";
        // code block
    }*/

    localStorage.setItem("IdCliente", id_cliente);

    var param = {
        nro_pedido: $('[name="NroPedido"]').val(),
        nro_ticket: $('[name="NroTicket"]').val(),
        id_cliente: parseInt($('#cmbCliente').val()) != 0 ? parseInt($('#cmbCliente').val()) : null,
        id_estado_pedido: parseInt($('#cmbCliente').val()) != 0 ? parseInt($('#cmbEstado').val()) : null,
        tipo_fecha: parseInt($('#cmbTipoFecha').val()),
        ff_desde: $('#dpFFDesde').val(),
        ff_hasta: $('#dpFFHasta').val(),
    };

    $("#tablaPedidos").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Home/LoadPedidos",
            "type": "POST",
            "data": { param },
            "datatype": "json"
        },
        "columns": [
            {
                "data": "nro_pedido", "name": "nro_pedido", "width": "10%", "searchable": true, "searchable": true,
                "render": function (nro_pedido) {
                    return '<strong> ' + nro_pedido + '</strong>';
                }
            },
            {
                "data": "nro_ticket", "name": "nro_ticket", "width": "10%", "searchable": true, "searchable": true,
                "render": function (nro_ticket) {
                    return '<strong> ' + nro_ticket + '</strong>';
                }
            },
            { "data": "ff_ingreso", "name": "ff_ingreso", "width": "12%", "searchable": true, "searchable": true },
            { "data": "ff_sla", "name": "ff_sla", "width": "12%", "searchable": true, "searchable": true },
            { "data": "ff_entrega", "name": "ff_entrega", "width": "12%", "searchable": false, "searchable": true },
            { "data": "ff_cumplimiento", "name": "ff_cumplimiento", "width": "14%", "searchable": false, "searchable": true },
            { "data": "sucursal", "name": "sucursal", "autoWidth": true, "searchable": false, "searchable": true },
            {
                "data": "descEstado", "name": "descEstado", "width": "10%", "searchable": true, "searchable": true,
                "render": function (descEstado) {
                    return '<span class="label label-default">' + descEstado + '</span>';
                }
            },           
            {
                "data": "id_pedido", "name": "id_pedido", "width": "8%", "searchable": false, "className": "text-center",
                "render": function (id_pedido) {
                    return '<a href="/DetallePedido/' + id_pedido + '" class="btn green btn-sm"><i class="fa fa-edit"></i> </a> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ pedidos por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron pedidos para los filtros seleccionados.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)",
            //"processing": $.tsw.showLoading("Espere", "#panelTablaPedidos"),
        },
        "destroy": true,
        "initComplete": function (settings) {
            $.tsw.hideLoading("#panelTablaPedidos");
            $('#aFiltros').click();
            if ($('#divResultados').css("display") === "none")
                $('#aResultado').click();
        }
    }); 
};
//-------------------------------------------------------------------------------------------
var Filtrar = function () {
    if ($('#cmbTipoFecha').val() != null && $('#dpFFDesde').val() == "")
        $.tsw.showMessageWarning("Ingrese fecha desde.");
    else if ($('#cmbTipoFecha').val() != null && $('#dpHasta').val() == "")
        $.tsw.showMessageWarning("Ingrese fecha hasta.");
    else if ($('#dpFFDesde').val() != "" && $('#dpFFHasta').val() == "")
        $.tsw.showMessageWarning("Ingrese fecha hasta.");
    else if ($('#cmbTipoFecha').val() == null && ($('#dpFFDesde').val() != "" || $('#dpFFHasta').val() != ""))
        $.tsw.showMessageWarning("Ingrese un tipo de fecha a filtrar.");
    else if ($.tsw.compareDates($('#dpFFDesde').val(), $('#dpFFHasta').val()) == 1)
        $.tsw.showMessageWarning("La fecha hasta no puede ser mayor a la fecha desde.");
    else
        LoadPedidos();
}
//-------------------------------------------------------------------------------------------
var ObtenerTotales = function () {
        $.ajax({
            type: "GET",
            url: "Home/ObtenerTotales",
            data: { id_cliente: id_cliente},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $('#txtInicial').text(data.data.nuevos);
                    $('#txtEnCurso').text(data.data.enCurso);
                    $('#txtFinal').text(data.data.finalizados);
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                //$.tsw.hideLoading("#PanelDatosContacto");
            }
        });
};
//-------------------------------------------------------------------------------------------
var Limpiar = function () {  
    $('[name="NroPedido"]').val("");
    $('[name="NroTicket"]').val("");
    $('#cmbCliente').val(0);
    $('#cmbEstado').val(0);
    $('#cmbTipoFecha').val(0);
    $('#dpFFDesde').val("");
    $('#dpFFHasta').val("");
}
//-------------------------------------------------------------------------------------------
//RETORNAR PEDIDO 
//-------------------------------------------------------------------------------------------
var retornarPedido = function (id) {
    $('#modalConfirmacion').modal('show');
    id_pedido = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.tsw.showLoading("Espere", "#panelTablaPedidos");
    $.ajax({
        type: "POST",
        url: "Home/RetornarPedido",
        data: { id: id_pedido, comentarios: $('#txtComentarios').val().trim() },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                    $('#tablaPedidos').dataTable()._fnReDraw();
                ObtenerTotales();
                LoadPedidos("I");
                $('#modalConfirmacion').modal('hide');
                $.tsw.showMessageOk(data.mensaje);
            }
            else if (data.code === 4)
                $.tsw.showMessageWarning(data.mensaje);
            else
                $.tsw.showMessageError(data.mensaje);
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#panelTablaPedidos");
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
});
//-------------------------------------------------------------------------------------------
/*if (performance.navigation.type == 2) {
    id_cliente = localStorage.getItem("IdCliente");
    $('#btnNuevo').prop('disabled', false);
    $('#tablaPedidos').dataTable().fnClearTable();
    ObtenerTotales();
    LoadPedidos("I");
}*/
//-------------------------------------------------------------------------------------------

