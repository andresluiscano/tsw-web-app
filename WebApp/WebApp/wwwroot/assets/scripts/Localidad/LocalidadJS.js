﻿//-------------------------------------------------------------------------------------------
var id_localidad = 0;
var id_provincia = 0;
//-------------------------------------------------------------------------------------------
//$.tsw.llenarCombos("cmbProvincia", "/Provincia/GetProvincias", "", "0");
//-------------------------------------------------------------------------------------------
var LoadLocalidades = function (id) {
    id_provincia = id;
    $.tsw.showLoading("Espere", "#PanelTablaLocalidad");
    $("#tabla").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Localidad/LoadData",
            "type": "POST",
            "data": { IdProvincia: id },
            "datatype": "json"
        },
        "columns": [
            { "data": "descripcion", "name": "descripcion", "autoWidth": true, "searchable": true },
            { "data": "latitud", "name": "latitud", "autoWidth": true, "searchable": false },
            { "data": "longitud", "name": "longitud", "autoWidth": true, "searchable": false },
            {
                "data": "id_localidad", "name": "id_localidad", "autoWidth": true, "searchable": false,
                "render": function (id_localidad) {
                    return '<button class="btn green btn-sm" onclick="editarL(' + id_localidad + ');"><i class="fa fa-edit"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ localidades por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron localidades para el cliente seleccionado.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#PanelTablaLocalidad");
        }
    });
    
}
//-------------------------------------------------------------------------------------------
$('#cmbProvincia').on('change', function () {
    if ($(this).val() != 0)
        LoadLocalidades($(this).val());
});
//-------------------------------------------------------------------------------------------
LoadLocalidades(0);
//-------------------------------------------------------------------------------------------
var editarL = function (id) {
    $.ajax({
        type: "GET",
        url: "/Localidad/GetLocalidad",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearForm();
            var p = data.localidad;
            id_localidad = p.id;
            $('[name="id_localidad"]').val(id_localidad);
            $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
            $('[name="Latitud"]').val(p.latitud).addClass(p.latitud == null ? "" : "edited");
            $('[name="Longitud"]').val(p.longitud).addClass(p.longitud == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
    });
}
//-------------------------------------------------------------------------------------------
var clearForm = function () {
    $("#form").validate().resetForm();
    //errorProvincia.hide();
    id_localidad = 0;
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('[name="Latitud"]').val("").removeClass("edited");
    $('[name="Longitud"]').val("").removeClass("edited");
    window.scrollTo(0, 0);
}
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
//-------------------------------------------------------------------------------------------
var enviarForm = function () {
    if ($("#form").validate().form()) {
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_localidad;
        param.IdProvincia = id_provincia;
        $.ajax({
            type: "POST",
            url: "/Localidad/EditLocalidad",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearForm();
                    $('#tabla').dataTable()._fnReDraw();
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
        });
    }
}