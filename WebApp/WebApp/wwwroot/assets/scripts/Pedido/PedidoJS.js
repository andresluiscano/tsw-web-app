﻿var id_pedido = 0;
var id_cliente = 0;
var cpsla = null;
var saltarValidacionCorrelativo = false;
$.tsw.llenarCombos("cmbProvincia", "/Provincia/GetProvincias", "", "0");
//-------------------------------------------------------------------------------------------
var clearModalPedido = function () {
    $('#modalNuevoPedido').modal('hide');
    $('[name="IdSucursal"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdContacto"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdProducto"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdModelo"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdConfiguracion"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="NroPedido"]').val("").removeClass("edited");
    $('#dpFFIngreso').val("").trigger('change');;
    $('#dpffSLA').val("").trigger('change');;
    $('#dpFFEnvioDiferido').val("").trigger('change');;
    $('[name="NroPedido"]').val("").removeClass("edited");
    $('[name="NroTicket"]').val("").removeClass("edited");
    $('[name="Cantidad"]').val("").removeClass("edited");
    $('[name="Comentarios"]').val("").removeClass("edited");
    $('[name="EnvioDiferido"]').val("0").removeClass("edited").trigger('change');
};
//-------------------------------------------------------------------------------------------
var getffSLA = function (ff, hh, idSuc, dias) {
    $.tsw.showLoading("Espere", "#formPedido");
    $.ajax({
        type: "GET",
        url: "Pedido/GetffSLA",
        data: { FFIngreso: ff, HHIngreso: hh, idSuc: idSuc, dias : dias > 0 ? dias : 0  },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {//OK
                $('[name="FFSLA"]').val(data.ff_sla).trigger('change');
                $('[name="cantidadDias"]').val(data.sla);
                //$('#dpffSLA').removeAttr('disabled');
            }
            else if (data.code === 4) {//No paso alguna validación
                $.tsw.showMessageWarning(data.mensaje);
                $('[name="FFSLA"]').val("")
                $('[name="cantidadDias"]').val("");
            } else { //Ocurrió una excepción
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#formPedido");
        }
    });
}
//-------------------------------------------------------------------------------------------
var enviarFormPedido = function (saltarValidacion = false) {

    var productos = getArrayProductos();

    if ($('#dpFFIngreso').val() == "")
        $.tsw.showMessageWarning("Ingrese la fecha de ingreso.");
    else if ($('#txtHoraIngreso').val() == "")
        $.tsw.showMessageWarning("Ingrese la hora de ingreso.");
    else if ($('[name="NroPedido"]').val().trim() == "")
        $.tsw.showMessageWarning("Ingrese Número de TSW.");
    /*else if ($('[name="IdSucursal"]').val() < 0 || $('[name="IdSucursal"]').val() == null || $('[name="IdSucursal"]').val() == undefined)
        $.tsw.showMessageWarning("Seleccione una sucursal.");
    else if ($('[name="IdContacto"]').val() < 0 || $('[name="IdContacto"]').val() == null || $('[name="IdContacto"]').val() == undefined)
        $.tsw.showMessageWarning("Seleccione un contacto.");*/
    /*else if ($('[name="NroTicket"]').val().trim() == "")
        $.tsw.showMessageWarning("Ingrese Número de ticket.");*/
    else if (productos.length === 0)
        $.tsw.showMessageWarning("Ingrese al menos un producto.");
   /* else if ($('[name="IdProveedorTransporte"]').val() < 0 || $('[name="IdProveedorTransporte"]').val() == null || $('[name="IdProveedorTransporte"]').val() == undefined)
        $.tsw.showMessageWarning("Ingrese el proveedor de transporte.");  
    else if ($('#dpffSLA').val() == "")
        $.tsw.showMessageWarning("Ingrese la fecha de cumplimiento.");*/
    else if ($('#dpffSLA').val() != "" && $.tsw.compareDates($('#dpFFIngreso').val(), $('#dpffSLA').val()) == 1)
        $.tsw.showMessageWarning("La fecha de cumplimiento no puede ser menor a la fecha de ingreso.");
    else {
        var param = {
            Id: id_pedido,
            NroPedido: $('[name="NroPedido"]').val(),
            IdSucursalCliente: $('#cmbSucursal').attr("IdSucursal") != undefined ? parseInt($('#cmbSucursal').attr("IdSucursal")) : 0,
            IdContactoSucursal: $('[name="IdContacto"]').val() != null ? parseInt($('[name="IdContacto"]').val()) : 0,
            TelefonoContacto: $('[name="TelefonoContacto"]').val() != "" ?  parseInt($('[name="TelefonoContacto"]').val()) : 0,
            EsNeutral: $('#cmbNeutral').val(),
            NroTicket: $('[name="NroTicket"]').val() != "" ? parseInt($('[name="NroTicket"]').val()) : 0,
            IdEquipo: $('[name="IdEquipo"]').val(),
            Comentarios: $('[name="Comentarios"]').val(),       
            IdProveedorTransporte: $('[name="IdProveedorTransporte"]').val() != null ? parseInt($('[name="IdProveedorTransporte"]').val()) : 0,
            FFIngreso: $('#dpFFIngreso').val().split(" ")[0] + " " + formatHour($('#txtHoraIngreso').val()),
            FFSLA: $('#dpFFSLA').val(),
            FFEnvioDiferido: $('#cmbEnvioDiferido').val() == 1 ? $('#dpFFEnvioDiferido').val() : "",
            ObservacionesEquipo: $('[name="ObservacionesEquipo"]').val(),  
            Equipo: $('[name="Estado"]').val(),  
            IdEstadoPedido: $('#cmbEstado').val()
        };

        if (saltarValidacionCorrelativo)
            saltarValidacion = saltarValidacionCorrelativo;

        $.ajax({
            url: "Pedido/EditPedido",
            type: "post",
            contentType: 'application/x-www-form-urlencoded',
            data: { param, productos, saltarValidacion },
            success: function (data) {
                if (data.code === 1) {
                    //$.tsw.showMessageOk(data.mensaje);
                    $('#strongNroPedido').text(data.datosPedido.nroPedido);
                    $("#strongFFSLA").text($('#dpFFSLA').val());
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else if (data.code === 5) {
                    $.tsw.showMessageWarning(data.mensaje);
                    saltarValidacionCorrelativo = true;
                }
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function (data) {
                //clearModalPedido();
                //console.log(data);
                if (data.responseJSON.code === 1) {
                    $('#modalPedidoNoCorrelativo').modal('hide');
                    window.setTimeout(function () {
                        $('#modalRegistroPedidoExitoso').modal('show', {
                            backdrop: 'static',
                            keyboard: false
                        })
                        //$('#modalRegistroPedidoExitoso').modal('show');
                    }, 500);
                }
                else if (data.responseJSON.code === 5)
                    window.setTimeout(function () {
                        $('#modalPedidoNoCorrelativo').modal('show', {
                            backdrop: 'static',
                            keyboard: false
                        })
                        //$('#modalPedidoNoCorrelativo').modal('show');
                    }, 500);               
            }
        });
    }
}
//-------------------------------------------------------------------------------------------
var editarSucursal = function () {
    $.tsw.showLoading("Espere", "#formPedido");
    $.ajax({
        type: "GET",
        url: "/Cliente/GetSucursalCliente",
        data: { id: $('#cmbSucursal').attr("IdSucursal") },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearFormSucursal();
            var s = data.sucursal;
            cpsla = data.codigoPostalSLA
            id_sucursal_cliente = s.id;
            $('[name="DescripcionSucursal"]').val(s.descripcionSucursal).addClass(s.descripcionSucursal == null ? "" : "edited");
            $('[name="IdProvincia"]').val(s.idProvincia).removeClass("edited").trigger('change');
            $.tsw.llenarCombos("cmbLocalidad", "/Localidad/GetLocalidades?id_provincia=" + s.idProvincia, "", s.idLocalidad, "", "0");
            $('[name="Direccion"]').val(s.direccion).addClass(s.direccion == null ? "" : "edited");
            $('[name="CodigoPostal"]').val(s.codigoPostal).addClass(s.codigoPostal == null ? "" : "edited");
            if (cpsla != null)
                $('[name="SLA"]').val(cpsla.sla).addClass(cpsla.sla == null ? "" : "edited");
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $('#modalSucursal').modal('show');
            $.tsw.hideLoading("#formPedido");
        }
    });
};
//-------------------------------------------------------------------------------------------
var clearFormSucursal = function () {
    $('#modalSucursal').modal('hide');
    id_sucursal_cliente = 0;
    $('[name="DescripcionSucursal"]').val("").removeClass("edited");
    $('[name="IdLocalidad"]').val("").removeClass("edited").trigger('change').empty();
    $('[name="IdProvincia"]').val("").removeClass("edited").trigger('change');
    $('[name="Direccion"]').val("").removeClass("edited");
    $('[name="CodigoPostal"]').val("").removeClass("edited");
    $('[name="SLA"]').val("").removeClass("edited");
    cpsla = null;
}
//-------------------------------------------------------------------------------------------
var enviarFormSucursal = function () {
    if ($("#form").validate().form()) {
        $.tsw.showLoading("Espere", "#PanelDatosSucursal");
        var param = objectifyForm($("#form").serializeArray());
        param.Id = id_sucursal_cliente;
        param.IdCliente = id_cliente;

        $.ajax({
            type: "POST",
            url: "/Cliente/EditSucursal",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                    clearFormSucursal();
                } else if (data.code === 4) {
                    $.tsw.showMessageWarning(data.mensaje);
                } else {
                    $.tsw.showMessageError(data.mensaje);
                }
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function () {
                $.tsw.hideLoading("#PanelDatosSucursal");
            }
        });
    }
}
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
};
//-------------------------------------------------------------------------------------------
//REFACTORING PRODUCTOS//
var indexRow = 1;

var addColToRowProductos = function () {

    var cmbProducto = $('#RowProductos').children("div[id^='Producto']:last-child").find("select[id^='cmbProducto']").val();
    var cantidad = $('#RowProductos').children("div[id^='Producto']:last-child").find("input[id^='txtCantidad']").val();
    //var marca = $('#RowProductos').children("div[id^='Producto']:last-child").find("select[id^='cmbMarca']").val();
    //var modelo = $('#RowProductos').children("div[id^='Producto']:last-child").find("select[id^='cmbModelo']").val();
    var configuracion = $('#RowProductos').children("div[id^='Producto']:last-child").find("select[id^='cmbConfiguracion']").val();

    if ((cmbProducto != null && cantidad != "") || (cmbProducto === undefined && cantidad === undefined)) {
        indexRow++;
        var col = '<div class="col-md-12" id="Producto' + indexRow + '">' +
                    '<div class="col-sm-4">' +
                        '<div class="form-group">' +
                            '<select id="cmbProducto' + indexRow + '" onchange="setConfiguracion(' + indexRow + ');" name="IdProducto' + indexRow + '" class="form-control select2" data-placeholder="Seleccione"></select>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-3">' +
                        '<div class="form-group">' +
                            '<select id="cmbConfiguracion' + indexRow + '" name="IdConfiguracion' + indexRow + '" class="form-control select2" data-placeholder="Seleccione"></select>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-2">' +
                        '<div class="form-group">' +
                            '<input class="form-control" name="Cantidad" id="txtCantidad' + indexRow + '" />' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-2">' +
                        '<div class="form-group">' +
                            '<input class="form-control" name="CantidadRetorno" id="txtRetorno' + indexRow + '" />' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-sm-1 text-right">' +
                        '<div class="form-group">' +
                            '<a class="btn red" id="deleteRow' + indexRow + '" onclick="removeColToRowProductos(' + indexRow + ');"><i class="fa fa-trash-o"></i> </a>' +
                        '</div>' +
                    '</div>' +
                '<div>';

        $(col).appendTo($('#RowProductos'));
        $.tsw.llenarCombos("cmbProducto" + indexRow, "/Producto/GetProductos", "", cmbProducto);
        $.tsw.llenarCombos("cmbConfiguracion" + indexRow, "/Producto/GetConfiguraciones", "", configuracion);
    } else {
        $.tsw.showMessageError("Los campos Producto y Cantidad son requeridos para el producto anterior.");
    }
}

var removeColToRowProductos = function (i) {
    $('#Producto' + i).remove();
}

var getArrayProductos = function () {
    var productos = [];
    $('#RowProductos').children("div[id^='Producto']").each(function () {
        if ($(this).find("select[id^='cmbProducto']").val() != "" && $(this).find("input[id^='txtCantidad']").val() != "") {
            var obj = {
                IdProducto: parseInt($(this).find("select[id^='cmbProducto']").val()),
                IdConfiguracionProducto: parseInt($(this).find("select[id^='cmbConfiguracion']").val()),
                Cantidad: parseInt($(this).find("input[id^='txtCantidad']").val()),
                CantidadRetorno: parseInt($(this).find("input[id^='txtCantidadRetorno']").val()),
                CantidadPurga: parseInt($(this).find("input[id^='txtCantidadPurga']").val()),
            }
            productos.push(obj);
        }
    });

    return productos;
}

var setConfiguracion = function (id) {
    var producto = $("select[id^='cmbProducto" + id + "']").val();
    if (producto === "3" || producto === "4")
        $("select[id^='cmbConfiguracion" + id + "']").val(-1).attr('disabled', 'disabled');
    else
        $("select[id^='cmbConfiguracion" + id + "']").attr('disabled', false);
}
//-------------------------------------------------------------------------------------------
var initializePedido = function () {
    $('#ffSLA, #ffEnvioDiferido').datetimepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        immediateUpdates: true,
        todayBtn: true,
        todayHighlight: true,
        pickerPosition: "bottom-left",
        disabledHours: true,
        pickTime: false,
        minView: "month",
        language: 'es',
    });

    $('#cmbSLA').on('change', function () {
        if ($('#dpFFIngreso').val() == "" )
            $.tsw.showMessageWarning("Ingrese la fecha de ingreso.");
        else if ($('#txtHoraIngreso').val() == "")
            $.tsw.showMessageWarning("Ingrese la hora de ingreso.");
        else if ($('#cmbSucursal').attr("IdSucursal") == undefined)
            $.tsw.showMessageWarning("Ingrese una sucursal.");
        else 
            getffSLA($('#dpFFIngreso').val(), $('#txtHoraIngreso').val(), $('#cmbSucursal').attr("IdSucursal"), parseInt($('#cmbSLA').val()));
    });
    $('#dpFFIngreso').val(moment().format("DD/MM/YYYY"));
    $('#txtHoraIngreso').timepicker({
        showInputs: true,
        defaultTime: moment().format("HH:mm"),
        template: 'dropdown',
        showMeridian: false,
        minuteStep: 5,
        explicitMode: true
    });
    //-------------------------------------------------------------------------------------------
    $('#cmbProveedorTransporte').prop('disabled', true);
    $('#cmbContacto').prop('disabled', true);
    //$('#dpFFEnvioDiferido').attr('disabled', 'disabled');
    //$('#dpFFIngreso').attr('disabled', 'disabled');
    //$('#dpffSLA').attr('disabled', 'disabled');
    //-------------------------------------------------------------------------------------------
    /*$('#cmbSucursal').on('change', function () {
        if ($(this).val() != 0 && $(this).val() != null && $(this).val() != undefined && $(this).val() != "") {
            $('#cmbContacto').prop('disabled', false);
            $('#cmbProveedorTransporte').prop('disabled', false);
            $.tsw.llenarCombos("cmbContacto", "Cliente/GetContactos?id_sucursal=" + $(this).attr("IdSucursal"), "", "0");
        }
    });*/
    //-------------------------------------------------------------------------------------------
    $('#cmbEnvioDiferido').on('change', function () {
        if ($(this).val() == 1)
            //$('#dpFFEnvioDiferido').removeAttr('disabled');
            $('#divFFEnvioDiferido').show();
        else
            //$('#dpFFEnvioDiferido').attr('disabled', 'disabled');
            $('#divFFEnvioDiferido').hide();
    });
    //-------------------------------------------------------------------------------------------
    $('#cmbProveedorTransporte').on('change', function () {
        if ($(this).val() == 1) {
            $('#dpFFIngreso').val(moment(new Date()).format('DD/MM/YYYY HH:mm'));
            if ($('#cmbSucursal').attr("IdSucursal") != "" && $('[name="IdProveedorTransporte"]').val() != null && $('#dpFFIngreso').val() != "" && $('#cmbContacto').val() != null)
                getffSLA($('#dpFFIngreso').val(), $('[name="IdProveedorTransporte"]').val(), $('#cmbSucursal').attr("IdSucursal"));
            //$('#dpFFIngreso').removeAttr('disabled').trigger('change');
        }/*else
            $('#dpFFIngreso').attr('disabled', 'disabled');*/
    });
    $('#cmbContacto').on('change', function () {
        if ($('#cmbSucursal').attr("IdSucursal") != "" && $('[name="IdProveedorTransporte"]').val() != null && $('#dpFFIngreso').val() != "" && $('#cmbContacto').val() != null)
            getffSLA($('#dpFFIngreso').val(), $('[name="IdProveedorTransporte"]').val(), $('#cmbSucursal').attr("IdSucursal"));
    });
    $('#cmbCliente').on('change', function () {
        id_cliente = parseInt($(this).val());
        $('#cmbSucursal').val("").prop('disabled', false);
    })
    //-------------------------------------------------------------------------------------------
    //$.tsw.llenarCombos("cmbSucursal", "/Cliente/GetSucursales?id_cliente=" + id_cliente, "", "0");
    $('#btnVerDatos').attr('disabled', 'disabled');
    $('#cmbSucursal').autocomplete({
        messages: {
            noResults: 'Sucursal inexistente.',
        },
        minLength: 2,
        delay: 500,
        source: function (request, response) {
            $.getJSON('/Cliente/BuscarSucursalesAutocomplete',
                {
                    id_cliente: id_cliente,
                    term: $('#cmbSucursal').val()
                }, response);
        },
        select: function (event, ui) {
            $('#cmbSucursal').attr("IdSucursal", ui.item.id);
            $('#cmbSucursal').val(ui.item.id);
            if ($('#cmbSucursal').val() != 0 && $('#cmbSucursal').val() != null && $('#cmbSucursal').val() != undefined && $('#cmbSucursal').val() != "") {
                $('#cmbContacto').prop('disabled', false);
                $('#cmbProveedorTransporte').prop('disabled', false);
                $.tsw.llenarCombos("cmbContacto", "Cliente/GetContactos?id_sucursal=" + $(this).attr("IdSucursal"), "", "0");
            };
            if ($('#cmbSucursal').val() == "") {
                $('#cmbContacto').prop('disabled', true).empty();
                $('#btnVerDatos').attr('disabled', 'disabled');
            } else {
                $('#btnVerDatos').attr('disabled', false);
                if ($('#dpFFIngreso').val() != "" && $('#txtHoraIngreso').val() != "" && $('#cmbSucursal').attr("IdSucursal") != undefined)
                    getffSLA($('#dpFFIngreso').val(), $('#txtHoraIngreso').val(), $('#cmbSucursal').attr("IdSucursal"));
            }
        },
        change: function (event, ui) {
            if ($('#cmbSucursal').val() == "") {
                $('#cmbSucursal').attr("IdSucursal", "");
                $('#cmbContacto').prop('disabled', true).empty();
                $('#btnVerDatos').attr('disabled', 'disabled');
            } else
                $('#btnVerDatos').attr('disabled', false);;
        }
    });
    //SECCIÓN INFORMACIÓN DEL PRODUCTO
    $.tsw.llenarCombos("cmbProducto1", "/Producto/GetProductos", "", "0");
    $.tsw.llenarCombos("cmbConfiguracion1", "/Producto/GetConfiguraciones", "", "0");
    //-------------------------------------------------------------------------------------------
    //$('#modalNuevoPedido').modal('show');
    $("#txtNroPedido, #dpFFIngreso, #txtTelefonoContacto, #txtHoraIngreso, #txtNroTicket, #dpFFSLA").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });
};
initializePedido();
//-------------------------------------------------------------------------------------------
function formatHour(hour) {
    var arr = hour.split(":");
    var hh = arr[0];
    var mm = arr[1];
    return ("0" + hh).substr(-2) + ":" + ("0" + mm).substr(-2);
}
//-------------------------------------------------------------------------------------------
var UpdateSLA = function () {
    $.tsw.showLoading("Espere", "#PanelDatosSucursal");
    var param = {};

    if (cpsla != null) {
        param = cpsla;
        param.sla = parseInt($('[name="SLA"]').val());
        param.ffBaja = null;
    } else {
        param.Id = 0;
        param.Sla = $('[name="SLA"]').val();
        param.CodigoPostal = $('[name="CodigoPostal"]').val();
    }

    $.ajax({
        type: "POST",
        url: "/CodigoPostalSLA/EditCodigoPostalSLA",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormSucursal();
                if ($('#dpFFIngreso').val() != "" && $('#txtHoraIngreso').val() != "" && $('#cmbSucursal').attr("IdSucursal") != undefined)
                    getffSLA($('#dpFFIngreso').val(), $('#txtHoraIngreso').val(), $('#cmbSucursal').attr("IdSucursal"));
            } else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosSucursal");
        }
    });
};