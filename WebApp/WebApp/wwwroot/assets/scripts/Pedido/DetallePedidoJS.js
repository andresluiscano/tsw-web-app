﻿var id_pedido_producto = 0;
var indexRow = 0;
//-------------------------------------------------------------------------------------------
var addColToRowProductos = function () {
    var row = $('#RowProductos').children("div[id^='Producto']:last-child");
    indexRow = $('#RowProductos').children("div[id^='Producto']").length;


    var cmbProducto = row.find("select[id^='cmbProducto']");

    if (cmbProducto === undefined)
        cmbProducto = row.find("input[id^='txtProducto']");
    var cantidad = row.find("input[id^='txtCantidad']");
    var configuracion = row.find("select[id^='cmbConfiguracion']");
    var producto = row.find("input[id^='txtIdPedidoProducto']");

    if ((cmbProducto.val() != null || cmbProducto.val() === undefined) && cantidad.val() != "") {
        indexRow++;
        var col = '<div class="col-md-12" id="Producto' + indexRow + '">' +
            '<input name="IdPedidoProducto" id="txtIdPedidoProducto" hidden />'+
            '<div class="col-sm-4">' +
            '<div class="form-group">' +
            '<select id="cmbProducto' + indexRow + '" onchange="setConfiguracion(' + indexRow + ');" name="IdProducto' + indexRow + '" class="form-control select2" data-placeholder="Seleccione"></select>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-3">' +
            '<div class="form-group">' +
            '<select id="cmbConfiguracion' + indexRow + '" name="IdConfiguracion' + indexRow + '" class="form-control select2" data-placeholder="Seleccione"></select>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="form-group">' +
            '<input class="form-control" name="Cantidad" id="txtCantidad' + indexRow + '" />' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="form-group">' +
            '<input class="form-control" name="CantidadRetorno" id="txtCantidadRetorno' + indexRow + '" />' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-1 text-right">' +
            '<div class="form-group">' +
            '<a class="btn red" id="deleteRow' + indexRow + '" onclick="removeColToRowProductosWithoutSave(' + indexRow + ');"><i class="fa fa-trash-o"></i> </a>' +
            '</div>' +
            '</div>' +
            '<div>';


        $.tsw.llenarCombos("cmbProducto" + indexRow, "/Producto/GetProductos", "", "0");
        $.tsw.llenarCombos("cmbConfiguracion" + indexRow, "/Producto/GetConfiguraciones", "", "0");

        $(col).appendTo($('#RowProductos'));
    } else {
        $.tsw.showMessageError("Los campos Producto y Cantidad son requeridos para el producto anterior.");
    }
};
//-------------------------------------------------------------------------------------------
var removeColToRowProductos = function (i) {
    //
    var producto = $("div[id^='Producto" + i + "']").find("input[id^='txtProducto']").val();
    var configuracion = $("div[id^='Producto" + i + "']").find("input[id^='txtConfiguracion']").val();
    id_pedido_producto = parseInt($("div[id^='Producto" + i + "']").find("input[id^='txtIdPedidoProducto']").val());
    indexRow = i;
    $('#productoSel').text(producto);
    $('#configuracionSel').text(configuracion);

    window.setTimeout(function () {
        $('#modalAvisoProductos').modal('show', {
            backdrop: 'static',
            keyboard: false
        });
    }, 500);
};
//-------------------------------------------------------------------------------------------
var getArrayProductos = function () {
    var productos = [];
    $('#RowProductos').children("div[id^='Producto']").each(function () {
        if ($(this).find("select[id^='cmbProducto']").val() != "" &&
            $(this).find("input[id^='txtCantidad']").val() != "" &&
            $(this).find("input[id^='txtIdPedidoProducto']").val() == "" ) {
            var obj = {
                IdProducto: parseInt($(this).find("select[id^='cmbProducto']").val()),
                IdConfiguracionProducto: parseInt($(this).find("select[id^='cmbConfiguracion']").val()),
                Cantidad: parseInt($(this).find("input[id^='txtCantidad']").val()),
                CantidadRetorno: parseInt($(this).find("input[id^='txtCantidadRetorno']").val()),
                CantidadPurga: parseInt($(this).find("input[id^='txtCantidadPurga']").val()),
            }
            productos.push(obj);
        }
    });

    return productos;
};
//-------------------------------------------------------------------------------------------
var eliminarProducto = function () {
    $.ajax({
        type: "POST",
        url: "/Pedido/EliminarProducto",
        data: { id: id_pedido_producto },
        contentType: 'application/x-www-form-urlencoded',
        success: function (data) {
            if (data.code === 1) {           
                $.tsw.showMessageOk(data.mensaje);
                $('#Producto' + indexRow).remove();
                id_pedido_producto = 0;
                indexRow = 0;
                $('#modalAvisoProductos').modal('hide');
                GetCostoProveedorCS();
            }
            else
                $.tsw.showMessageError(data.mensaje);
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function (data) {
            $('html, body').animate({ scrollTop: 0 }, 'fast');
        }
    });
};
//-------------------------------------------------------------------------------------------
var removeColToRowProductosWithoutSave = function (i) {
    $('#Producto' + i).remove();
};
//-------------------------------------------------------------------------------------------
var setConfiguracion = function (id) {
    var producto = $("select[id^='cmbProducto" + id + "']").val();
    if (producto === "3" || producto === "4")
        $("select[id^='cmbConfiguracion" + id + "']").val(-1).attr('disabled', 'disabled');
    else
        $("select[id^='cmbConfiguracion" + id + "']").attr('disabled', false);
};
//-------------------------------------------------------------------------------------------
var GuardarPedido = function () {

    var productos = getArrayProductos();
    var id_estado_pedido = $('#cmbEstado').val();

    /*if (productos.length === 0)
        $.tsw.showMessageWarning("Para guardar los cambios debe ingresar al menos un producto nuevo.");
    else {*/
        var id_pedido = Number($('#txtIdPedido').val());
        $.ajax({
            url: "/Pedido/GuardarPedido",
            type: "POST",
            data: { id_pedido, productos, id_estado_pedido },
            contentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                if (data.code === 1) {
                    $.tsw.showMessageOk(data.mensaje);
                }
                else if (data.code === 4)
                    $.tsw.showMessageWarning(data.mensaje);
                else
                    $.tsw.showMessageError(data.mensaje);
            },
            error: function (data) {
                $.tsw.showMessageError(data.mensaje);
            },
            complete: function (data) {
                location.reload();
                //$('html, body').animate({ scrollTop: 0 }, 'fast');
                $.tsw.showMessageOk(data.responseJSON.mensaje);
                //clearModalPedido();
                //console.log(data);
                /*if (data.responseJSON.code === 1)
                    window.setTimeout(function () {
                        $('#modalRegistroPedidoExitoso').modal('show', {
                            backdrop: 'static',
                            keyboard: false
                        })
                        //$('#modalRegistroPedidoExitoso').modal('show');
                    }, 500);*/
            }
        });
    //}
}
//-------------------------------------------------------------------------------------------
var GetCostoProveedorCS = function () {
    $.tsw.showLoading("Espere", "#divCalculoCS");
    var id_pedido = Number($('#txtIdPedido').val());
    $.ajax({
        url: "/Transporte/GetCostoProveedorCS",
        type: "GET",
        data: { id: id_pedido },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                //$.tsw.showMessageOk(data.mensaje);
                $('#txtPesoVolumetricoTotal').text(parseFloat(data.pesoVolumetricoTotal));
                $('#txtPesoRealTotal').text(parseFloat(data.pesoRealTotal));
                $('#txtCalculoRadial').text(parseFloat(data.calculoRadial));
                $('#txtMontoPorValorDeclaradoTotal').text(parseFloat(data.montoPorValorDeclaradoTotal));
                $('#txtValorPorRemito').text(parseFloat(data.valorPorRemito));
                $('#txtMontoTotal').text(parseFloat(data.montoTotal));
            }
            else if (data.code === 4)
                $.tsw.showMessageWarning(data.mensaje);
            else
                $.tsw.showMessageError(data.mensaje);
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function (data) {
            $.tsw.hideLoading("#divCalculoCS");
            //clearModalPedido();
            //console.log(data);
            /*if (data.responseJSON.code === 1)
                window.setTimeout(function () {
                    $('#modalRegistroPedidoExitoso').modal('show', {
                        backdrop: 'static',
                        keyboard: false
                    })
                    //$('#modalRegistroPedidoExitoso').modal('show');
                }, 500);*/
        }
    });
}