﻿var id_circuito_radial = 0;
//-------------------------------------------------------------------------------------------
var LoadCircuitoRadial = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosCircuito");
    $("#tablaCircuitoRadial").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Transporte/LoadCircuitosRadiales",
            "type": "POST",
            "data": { id_proveedor_transporte: id },
            "datatype": "json"
        },
        "columns": [
            { "data": "escala", "name": "escala", "autoWidth": true, "searchable": true, "className": "text-center"},
            {
                "data": "valor", "name": "valor", "autoWidth": true, "searchable": true, "className": "text-center",
                "render": function (data, type, full) {
                    return "$ " + full.valor;
                }
            },
            {
                "data": "valor_adicional", "name": "valor_adicional", "autoWidth": true, "searchable": true, "className": "text-center",
                "render": function (data, type, full) {
                    return "$ " + full.valor_adicional;
                }
            },
            {
                "data": "id_circuito_radial", "name": "id_circuito_radial", "autoWidth": true, "searchable": false, "className": "text-center",
                "render": function (id_circuito_radial) {
                    return '<button class="btn green btn-sm" onclick="editarCircuito(' + id_circuito_radial + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarCircuito(' + id_circuito_radial + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ circuitos radiales por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron circuito radiales para el proveedor seleccionado.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#PanelDatosCircuito");
        }
    });
}
//-------------------------------------------------------------------------------------------
var clearFormCircuito = function () {
    $('#modalCircuito').modal('hide');
    id_circuito_radial = 0;
    $('[name="Escala"]').val("").removeClass("edited");
    $('[name="Valor"]').val("").removeClass("edited");
    $('[name="ValorAdicional"]').val("").removeClass("edited").trigger('change').empty();
    window.scrollTo(0, 0);
}
//-------------------------------------------------------------------------------------------
var enviarFormCircuito = function () {
    $.tsw.showLoading("Espere", "#PanelDatosCircuito");

    var param = {
        Id: id_circuito_radial,
        IdProveedorTransporte: id_proveedor_transporte,
        Escala: parseInt($('#txtEscalaCR').val()),
        Valor: parseFloat($('#txtValor').val().replace(',','.')),
        ValorAdicional: parseFloat($('#txtValorAdicional').val().replace(',', '.'))
    };


    $.ajax({
        type: "POST",
        url: "/Transporte/EditCircuitoRadial",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormCircuito();
                $('#tablaCircuitoRadial').dataTable()._fnReDraw();
            }
            else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            }
            else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosCircuito");
        }
    });
}
//-------------------------------------------------------------------------------------------
var editarCircuito = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosCircuito");
    $.ajax({
        type: "GET",
        url: "/Transporte/GetCircuitoRadial",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearFormCircuito();
            var s = data.circuitoRadial;
            id_circuito_radial = s.id;
            $('[name="EscalaCR"]').val(s.escala).addClass(s.escala == null ? "" : "edited");
            $('[name="Valor"]').val(s.valor).addClass(s.valor == null ? "" : "edited");
            $('[name="ValorAdicional"]').val(s.valorAdicional).addClass(s.valorAdicional == null ? "" : "edited");
            $('#modalCircuito').modal('show');
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosCircuito");
        }
    });
};
//-------------------------------------------------------------------------------------------
$('#modalCircuito').on('hidden.bs.modal', function () {
    clearFormCircuito();
});
//------------------------------------------------------------------------------------------
var eliminarCircuito = function (id) {
    $('#modalConfirmacion').modal('show');
    id_circuito_radial = id;
};
$("#confirmacionAceptar").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Transporte/EliminarCircuitoRadial",
        data: { id: id_circuito_radial },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormCircuito();
                $('#modalConfirmacion').modal('hide');
                $('#tablaCircuitoRadial').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
});
$("#confirmacionCancelar").on("click", function (e) {
    $('#modalConfirmacion').modal('hide');
    clearFormCircuito();
});