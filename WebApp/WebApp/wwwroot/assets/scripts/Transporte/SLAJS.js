﻿//-------------------------------------------------------------------------------------------
$.tsw.llenarCombos("cmbProvincia", "/Provincia/GetProvincias", "", "0");
//-------------------------------------------------------------------------------------------
var id_sla_transporte = 0;
//-------------------------------------------------------------------------------------------
var LoadSLA = function (id) {
    $.tsw.showLoading("Espere", "#PanelTablaSLA");
    $("#tablaSLA").DataTable({
        "processing": false,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "search": true,
        "ajax": {
            "url": "/Transporte/LoadSLA",
            "type": "POST",
            "data": { id_proveedor_transporte: id },
            "datatype": "json"
        },
        "columns": [
            //{ "data": "compromiso_entrega", "name": "compromiso_entrega", "width": "10%", "searchable": true, "className": "text-left" },
            { "data": "codigo_postal", "name": "codigo_postal", "width": "40%", "searchable": true, "className": "text-left" },
            { "data": "escala", "name": "escala", "width": "40%", "searchable": true, "className": "text-left" },       
            //{ "data": "localidad", "name": "localidad", "autoWidth": true, "searchable": true, "className": "text-left" },
            {
                "data": "id_sla_transporte", "name": "id_sla_transporte", "width": "20%", "searchable": false, "className": "text-center",
                "render": function (id_sla_transporte) {
                    return '<button class="btn green btn-sm" onclick="editarSLA(' + id_sla_transporte + ');"><i class="fa fa-edit"></i> </button> <button class="btn red btn-sm" onclick="eliminarSLA(' + id_sla_transporte + ');"><i class="fa fa-trash-o"></i> </button>';
                }
            }
        ],
        "language": {
            "lengthMenu": "Mostrando _MENU_ SLA por página.",
            "search": "Filtrar por:",
            "zeroRecords": "<br/><div class='alert alert-warning'><strong>Atención!</strong> No se encontraron SLAs para el proveedor seleccionado.</div>",
            "info": "Mostrando _PAGE_ de _PAGES_ páginas.",
            "infoEmpty": "",
            "infoFiltered": "Filtrando _MAX_ del total de registros.)"
        },
        "destroy": true,
        "drawCallback": function (settings) {
            $.tsw.hideLoading("#PanelTablaSLA");
        }
    });
}
//-------------------------------------------------------------------------------------------
$('#cmbProvincia').on('change', function () {
    if ($(this).val() != 0 && $(this).val() != null && $(this).val() != undefined)
        $.tsw.llenarCombos("cmbLocalidad", "/Localidad/GetLocalidades?id_provincia=" + $(this).val(), "", "0");
});
//-------------------------------------------------------------------------------------------
var clearFormSLA = function () {
    $('#modalSLA').modal('hide');
    id_sla_transporte = 0;
    //$('[name="IdProveedorTransporte"]').val("").removeClass("edited");
    //$('[name="CompromisoEntrega"]').val("").removeClass("edited");
    $('[name="Escala"]').val("").removeClass("edited");
    //$('[name="DescripcionSLA"]').val("").removeClass("edited");
    //$('[name="IdLocalidad"]').val("").removeClass("edited").trigger('change').empty();
    //$('[name="IdProvincia"]').val("").removeClass("edited").trigger('change');
    $('[name="CodigoPostal"]').val("").removeClass("edited");
    window.scrollTo(0, 0);
}
//-------------------------------------------------------------------------------------------
var enviarFormSLA = function () {
    $.tsw.showLoading("Espere", "#PanelDatosSLA");
    var param = objectifyForm($("#form").serializeArray());
    param.Id = id_sla_transporte;
    param.IdProveedorTransporte = id_proveedor_transporte;
    $.ajax({
        type: "POST",
        url: "/Transporte/EditSLA",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormSLA();
                $('#tablaSLA').dataTable()._fnReDraw();
            }
            else if (data.code === 4) {
                $.tsw.showMessageWarning(data.mensaje);
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosSLA");
        }
    });
}
//-------------------------------------------------------------------------------------------
var editarSLA = function (id) {
    $.tsw.showLoading("Espere", "#PanelDatosSLA");
    $.ajax({
        type: "GET",
        url: "/Transporte/GetSLA",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            clearFormSLA();
            var s = data.sla;
            //$('[name="IdProvincia"]').val(s.idProvincia).removeClass("edited").trigger('change');
            id_sla_transporte = s.id;
            //$('[name="CompromisoEntrega"]').val(s.compromisoEntrega).addClass(s.compromisoEntrega == null ? "" : "edited");
            $('[name="Escala"]').val(s.escala).addClass(s.escala == null ? "" : "edited");         
            //$.tsw.llenarCombos("cmbLocalidad", "/Localidad/GetLocalidades?id_provincia=" + s.idProvincia, "", s.idLocalidad, "", "0");
            $('[name="CodigoPostal"]').val(s.codigoPostal).addClass(s.codigoPostal == null ? "" : "edited");          
            $('#modalSLA').modal('show');
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function () {
            $.tsw.hideLoading("#PanelDatosSLA");
        }
    });
};
//-------------------------------------------------------------------------------------------
$('#modalSLA').on('hidden.bs.modal', function () {
    clearFormSLA();
});
//-------------------------------------------------------------------------------------------
var newSLA = function () {
    $('[name="IdProvincia"]').val("").removeClass("edited").trigger('change');
    $('#modalSLA').modal('show');
}
//-------------------------------------------------------------------------------------------
var objectifyForm = function (formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
};
//-------------------------------------------------------------------------------------------
var eliminarSLA = function (id) {
    $('#modalConfirmacionSLA').modal('show');
    id_sla_transporte = id;
};
$("#confirmacionAceptarSLA").on("click", function (e) {
    $.ajax({
        type: "POST",
        url: "/Transporte/EliminarSLA",
        data: { id: id_sla_transporte },
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {
                $.tsw.showMessageOk(data.mensaje);
                clearFormSLA();
                $('#modalConfirmacionSLA').modal('hide');
                $('#tablaSLA').dataTable()._fnReDraw();
            } else {
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
    });
});
$("#confirmacionCancelarSLA").on("click", function (e) {
    $('#modalConfirmacionSLA').modal('hide');
    clearFormSLA();
});
//------------------------------------------------------------------------------------------
$("input.number").keypress(function (event) {
    return /\d/.test(String.fromCharCode(event.keyCode));
});