﻿var id_proveedor_transporte;
var FF_Alta;
var logo_proveedor;
//-------------------------------------------------------------------------------------------
var enviarForm = function (esBaja) {
    var param = {
        Id: id_proveedor_transporte,
        Descripcion: $("#descripcion").val(),
        ValorMaximoPermitido: parseFloat($("#ValorMaximoPermitido").val()),
        ValorPorRemito: parseFloat($("#ValorPorRemito").val()),
        ff_alta: ff_alta,
        esBaja: esBaja,
        LogoProveedor: logo_proveedor
    };

    $.ajax({
        type: "POST",
        url: "/Transporte/EditProveedor",
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code === 1) {//Edito OK
                $.tsw.showMessageOk(data.mensaje);
                //buscarProveedor(param.Descripcion);
            }
            else if (data.code === 2) {//Borra lógicamente OK
                $.tsw.showMessageWarning(data.mensaje);
                $("avisoBusqueda").show();
                initialize();
            } else if (data.code === 4) {//No se paso alguna validación
                $.tsw.showMessageWarning(data.mensaje);
            } else { //Ocurrió una excepción
                $.tsw.showMessageError(data.mensaje);
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        },
        complete: function (data) {
            location.reload();
            $('html, body').animate({ scrollTop: 0 }, 'fast');
        }
    });
}
//-------------------------------------------------------------------------------------------
var buscarProveedor = function (id) {
    $.ajax({
        type: "GET",
        url: "/Transporte/GetProveedor",
        data: { id: id },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.code == 1) { //Existe el cliente
                /*$("#btnGuardar").addClass("hidden");
                $("#btnCancelar").addClass("hidden");
                $("#btnEditar").removeClass("hidden");
                $("#btnBorrar").removeClass("hidden");
                $("#btnBorrar").text("Borrar");
                $("#btnBorrar").removeClass("grey-salsa eliminar").addClass("red");
                $("#btnNuevo").addClass("hidden");
                $("#btnBorrar").addClass("eliminar");
                $("#avisoBusqueda").hide();

                var p = data.proveedor;
                id_proveedor_transporte = p.id;
                logo_proveedor = p.logoProveedor != null ? p.logoProveedor : '/assets/img/profile_user.jpg';
                ff_alta = p.ff_alta;
                $('[name="id_proveedor_transporte"]').val(id_proveedor_transporte);
                $('[name="Descripcion"]').val(p.descripcion).addClass(p.descripcion == null ? "" : "edited");
                $('#descripcionNoEditable').text(p.descripcion);
                $('#DivDescripcionNoEditable').show();
                $('#DivDescripcionEditable').hide();
                $('#logo_proveedor').attr('src', logo_proveedor);*/

                if (id_proveedor_transporte != 0) {
                    $("#btnGuardar").addClass("hidden");
                    //$("#btnBorrar").removeClass("hidden");
                    $("#btnCancelar").addClass("hidden");
                    $("#btnEditar").removeClass("hidden");
                    $("#btnNuevo").addClass("hidden");
                } else {
                    $("#btnNuevo").removeClass("hidden");
                    $("#btnGuardar").addClass("hidden");
                    //$("#btnBorrar").addClass("hidden");
                    $("#btnCancelar").addClass("hidden");
                    $("#btnEditar").addClass("hidden");
                    //$('#logo_proveedor').attr('src', '/assets/img/profile_user.jpg');
                }
                $("#inputFile").attr("disabled", true);
                $('#DivDescripcionEditable').hide();
                $('#DivDescripcionNoEditable').show();
                $("#avisoImagen").hide();
                $('#DivValorMaximoPermitidoEditable').hide();
                $('#DivValorPorRemitoEditable').hide();
                $("#avisoBusqueda").hide();

                var p = data.proveedor;
                //SLA
                clearFormSLA();
                if (!$.fn.DataTable.isDataTable('#tablaSLA')) {
                    LoadSLA(p.id);
                }
                else {
                    $('#tablaSLA').dataTable().fnClearTable();
                    LoadSLA(p.id);
                }
                //CircuitoRadial
                clearFormCircuito();
                if (!$.fn.DataTable.isDataTable('#tablaCircuitoRadial')) {
                    LoadCircuitoRadial(p.id);
                }
                else {
                    $('#tablaCircuitoRadial').dataTable().fnClearTable();
                    LoadCircuitoRadial(p.id);
                }

                $("#nuevaSLA").removeAttr('disabled');
                $("#nuevoCircuitoRadial").removeAttr('disabled');
            } else {//No se encontró el cliente
                $("#avisoBusqueda").show();
                initialize();
            }
        },
        error: function (data) {
            $.tsw.showMessageError(data.mensaje);
        }
    });
}
//-------------------------------------------------------------------------------------------
$("#btnNuevo").on("click", function (e) {
    $("#btnEditar").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $(this).addClass("hidden");
    $('#DivDescripcionEditable').show();
    $('#DivDescripcionNoEditable').hide();
    $("#btnBorrar").addClass("eliminar");
    $("#avisoBusqueda").hide();
    $("#avisoImagen").show();
    $("#cmbProveedorTransporte").val(0);
    $("#inputFile").attr("disabled", false);
});
//-------------------------------------------------------------------------------------------
$("#btnEditar").on("click", function (e) {
    $('#descripcion').val($('#descripcionNoEditable').text().trim());
    $('#ValorMaximoPermitido').val(parseFloat($('#ValorMaximoPermitidoNoEditable').text().trim().split(' ')[0].replace(/,/g, '.')).toFixed(2));
    $('#ValorPorRemito').val(parseFloat($('#ValorPorRemitoNoEditable').text().trim().replace(/,/g, '.')).toFixed(2));
    $("#btnNuevo").addClass("hidden");
    $("#btnGuardar").removeClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $(this).addClass("hidden");
    $("#btnCancelar").removeClass("hidden");
    $('#DivDescripcionEditable').show();
    $('#DivDescripcionNoEditable').hide();
    $('#DivValorMaximoPermitidoEditable').show();
    $('#DivValorPorRemitoEditable').show();
    $("#inputFile").attr("disabled", false);
    $("#avisoImagen").show();
});
/*$("#cmbProveedorTransporte").on("change", function () {
    if ($(this).val() != 0 && $(this).val() != null)
        buscarProveedor(parseInt($("#cmbProveedorTransporte").val()));
});*/
$(".eliminar").on("click", function (e) {
    enviarForm(true);
    initialize();
});
$(".cancelar").on("click", function (e) {
    if (id_proveedor_transporte != 0) {
        $("#btnGuardar").addClass("hidden");
        //$("#btnBorrar").removeClass("hidden");
        $("#btnCancelar").addClass("hidden");
        $("#btnEditar").removeClass("hidden");
        $("#btnNuevo").addClass("hidden");
    } else {
        $("#btnNuevo").removeClass("hidden");
        $("#btnGuardar").addClass("hidden");
        //$("#btnBorrar").addClass("hidden");
        $("#btnCancelar").addClass("hidden");
        $("#btnEditar").addClass("hidden");
        //$('#logo_proveedor').attr('src', '/assets/img/profile_user.jpg');
    }
    $("#inputFile").attr("disabled", true);
    $('#DivDescripcionEditable').hide();
    $('#DivDescripcionNoEditable').show();
    $("#avisoImagen").hide();
    $('#DivValorMaximoPermitidoEditable').hide();
    $('#DivValorPorRemitoEditable').hide();
});
$("#logo_proveedor").on("click", function (e) {
    $("#inputFile").click();
})
//-------------------------------------------------------------------------------------------
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#logo_proveedor').attr('src', e.target.result);
            logo_proveedor = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#inputFile").change(function () {
    readURL(this);
});
//-------------------------------------------------------------------------------------------
var initialize = function () {
    id_proveedor_transporte = parseInt($('#id_proveedor_transporte').val());
    ff_alta = $('#ff_alta').val();
    logo_proveedor = $('#logo_proveedor').attr('src');
    /*$("#btnGuardar").addClass("hidden");
    $("#btnBorrar").addClass("hidden");
    $("#btnCancelar").addClass("hidden");
    $("#btnEditar").addClass("hidden");
    $("#btnNuevo").removeClass("hidden");
    $('#descripcionNoEditable').text("");
    $('[name="Descripcion"]').val("").removeClass("edited");
    $('#DivDescripcionNoEditable').show();
    $('#DivDescripcionEditable').hide();

    $('#logo_proveedor').attr('src', '/assets/img/profile_user.jpg');
    $("#inputFile").attr("disabled", true);
    $("avisoBusqueda").hide();
    $("#avisoImagen").hide();
    $("#nuevaSLA").attr('disabled', 'disabled');
    $("#nuevoCircuitoRadial").attr('disabled', 'disabled');*/
    //$.tsw.llenarCombos("cmbProveedorTransporte", "Transporte/GetProveedores", "", "0");
    //$("#inputCliente").val("");
    buscarProveedor(1);
}
initialize();