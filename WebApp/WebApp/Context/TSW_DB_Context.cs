﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Context
{
    public class TSW_DB_Context : DbContext
    {
        public TSW_DB_Context(DbContextOptions<TSW_DB_Context> options) : base(options)
        {

        }
        public DbSet<Provincia> provincia { get; set; }
        public DbSet<Localidad> localidad { get; set; }
        public DbSet<Cliente> cliente { get; set; }
        public DbSet<SucursalCliente> sucursal_cliente { get; set; }
        public DbSet<ContactoSucursal> contacto_sucursal { get; set; }
        public DbSet<Producto> producto { get; set; }
        public DbSet<ModeloProducto> modelo_producto { get; set; }
        public DbSet<ConfiguracionProducto> configuracion_producto { get; set; }
        public DbSet<MarcaProducto> marca_producto { get; set; }
        public DbSet<EstadoStock> estado_stock { get; set; }
        public DbSet<EstadoPedido> estado_pedido { get; set; }
        public DbSet<ProveedorTransporte> proveedor_transporte { get; set; }
        public DbSet<SLATransporte> sla_transporte { get; set; }
        public DbSet<CircuitoRadial> circuito_radial { get; set; }
        public DbSet<Pedido> pedido { get; set; }
        public DbSet<PedidoProducto> pedido_producto { get; set; }      
        public DbSet<Feriado> feriado { get; set; }
        public DbSet<CodigoPostalSLA> codigo_postal_sla { get; set; }
        public DbSet<LogError> log_error { get; set; }
    }
}
