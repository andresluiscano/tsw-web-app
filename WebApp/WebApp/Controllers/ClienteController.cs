﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models.DTO;
using System.Linq.Dynamic.Core;
using WebApp.Models;


namespace WebApp.Controllers
{
    public class ClienteController : BaseController
    {

        public ClienteController(TSW_DB_Context _context) : base(_context) {}

        #region CLIENTE
        [Route("Parametrización/Cliente")]
        [Obsolete]
        public IActionResult ClienteView()
        {
            try
            {
                List<Cliente> clientes = _context.cliente
                          .Where(c => c.ff_baja == null)
                          .OrderBy(c => c.id_cliente)
                          .ToList();

                ViewData["clientes"] = clientes;

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iParametrizacion", clase = "active" });
                menu.Add(new NodoMenu() { id = "iCliente", clase = "active" });

                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex) {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetCliente(int id)
        {
            try
            {
                var c = _context.cliente.Where(i => i.id_cliente == id && i.ff_baja == null).FirstOrDefault();

                var cliente = c != null ? new ClienteDTO(c) : null;
                
                return Json(new { Code = c != null ? 1 : 2, Cliente = cliente });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditCliente([FromBody] ClienteDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //Validaciones en Alta y Modificación
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var cli = param.GetCliente();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones en baja
                    if (param.esBaja)
                        if (PuedeDarDeBajaCliente(cli.id_cliente))                        
                            cli.ff_baja = DateTime.Now;
                        else
                            return Json(new { Code = 4, Mensaje = "No es posible borrar el cliente." });
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.cliente.Add(cli);
                        msg = "Cliente añadido exitosamente.";
                    }
                    else
                    {
                        _context.cliente.Attach(cli);
                        _context.Entry(cli).State = EntityState.Modified;
                        msg = !param.esBaja ? "Cliente actualizado exitosamente." : "Cliente eliminado exitosamente."; ;
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = !param.esBaja ? 1: 2, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetClientes()
        {
            try
            {
                var data = _context.cliente
                              .Where(c => c.ff_baja == null)
                              .Select(c => new { id = c.id_cliente, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un cliente" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [Obsolete]
        public bool PuedeDarDeBajaCliente(int id)
        {
            try
            {
                var cliente = _context.sucursal_cliente
                              .Where(sc => sc.id_cliente == id).ToList();
                var pedido = from p in _context.pedido
                             join s in _context.sucursal_cliente on p.id_sucursal_cliente equals s.id_sucursal_cliente
                             where s.id_cliente == id
                             select p;

                if (cliente.Count() > 0 && pedido.Count() > 0)
                    return cliente.All(sc => sc.ff_baja != null) && pedido.All(sc => sc.ff_baja != null);
                else if (cliente.Count() == 0 && pedido.Count() > 0)
                    return pedido.All(sc => sc.ff_baja != null);
                else if (cliente.Count() > 0 && pedido.Count() == 0)
                    return cliente.All(sc => sc.ff_baja != null);
                else
                    return true;
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return false;
            }
        }
        #endregion

        #region SUCURSALES
        [HttpPost]
        [Obsolete]
        public IActionResult LoadSucursales(int id_cliente)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var sucursales = (from sc in _context.sucursal_cliente
                                  join l in _context.localidad on sc.id_localidad equals l.id_localidad
                                  where sc.id_cliente == id_cliente && sc.ff_baja == null
                                  select 
                                    new { 
                                        id_sucursal_cliente = sc.id_sucursal_cliente,
                                        descripcion = sc.descripcion,
                                        direccion = sc.direccion,
                                        codigo_postal = sc.codigo_postal,
                                        localidad = l.descripcion });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sucursales = sucursales.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    sucursales = sucursales.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = sucursales.Count();
                //Paging   
                var data = sucursales.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditSucursal([FromBody] SucursalDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    if (!ModelState.IsValid){
                        Dictionary<string, int> hash = new Dictionary<string, int>(); //El hashmap sirve para ordenar las validaciones
                        hash.Add("CODIGOSUCURSAL", 1);
                        hash.Add("DESCRIPCIONSUCURSAL", 2);
                        hash.Add("IDLOCALIDAD", 3);
                        hash.Add("CALLE", 4);
                        hash.Add("CODIGOPOSTAL", 5);

                        var message = ModelState.Select(x => new { Key = hash[x.Key.ToUpper()], Errors = x.Value.Errors}).OrderBy(x=> x.Key);
                        
                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault().Errors.Select(e => e.ErrorMessage) });
                    }
                    //-----------------------------------------------------------------------------------------------
                    string msg = "";
                    var suc = param.GetSucursalCliente();
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        suc.ff_alta = DateTime.Today;
                        _context.sucursal_cliente.Add(suc);
                        msg = "Sucursal añadida exitosamente.";
                    }
                    else
                    {
                        SucursalCliente  s = _context.sucursal_cliente.Where(x => x.id_sucursal_cliente == suc.id_sucursal_cliente).SingleOrDefault();
                        s.codigo_postal = suc.codigo_postal;
                        s.descripcion = suc.descripcion;
                        s.direccion = suc.direccion;
                        s.id_localidad = suc.id_localidad;
                        _context.sucursal_cliente.Attach(s);
                        _context.Entry(s).State = EntityState.Modified;
                        msg = "Sucursal actualizada exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }


        [HttpGet]
        [Obsolete]
        public IActionResult GetSucursalCliente(int id)
        {
            try
            {
                var s = (from sc in _context.sucursal_cliente
                         join l in _context.localidad on sc.id_localidad equals l.id_localidad
                         where sc.id_sucursal_cliente == id                       
                             select new SucursalClienteInterno() { 
                                id_sucursal_cliente = sc.id_sucursal_cliente,
                                id_localidad = sc.id_localidad,
                                id_provincia = l.id_provincia,
                                descripcion = sc.descripcion,
                                direccion = sc.direccion,
                                codigo_postal = sc.codigo_postal
                             }
                         ).FirstOrDefault();

                //s.sla = Int32.TryParse(_context.codigo_postal_sla.FirstOrDefault(x => x.codigo_postal.ToString() == s.codigo_postal)?.sla.ToString(), out int value) ? (int) value : 0;

                var sucursal = s != null ? new SucursalDTO(s) : null;
                CodigoPostalSLA cpsla = s != null ? _context.codigo_postal_sla.FirstOrDefault(x => x.codigo_postal.ToString() == s.codigo_postal && x.ff_baja == null) : null;

                return Json(new { Code = s != null ? 1 : 2, Sucursal = sucursal, CodigoPostalSLA = (cpsla != null ? new CodigoPostalSLADTO(cpsla) : null )});
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetSucursales(int id_cliente)
        {
            try
            {
                var data = _context.sucursal_cliente
                                .Where(sc => sc.id_cliente == id_cliente && sc.ff_baja == null)
                                .Select(c => new { id = c.id_sucursal_cliente, text = $"{c.descripcion} ({c.direccion})", })
                                .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione una sucursal" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarSucursal(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var s = _context.sucursal_cliente.FirstOrDefault(u => u.id_sucursal_cliente == id);

                    string msg = "";
                    int code = 1;

                    if (PuedeDarDeBajaSucursal(id))
                    {
                        s.ff_baja = DateTime.Now;
                        _context.sucursal_cliente.Attach(s);
                        _context.Entry(s).State = EntityState.Modified;
                        msg = "Sucursal eliminada exitosamente.";
                    }
                    else {
                        code = 2;
                        msg = "No es posible eliminar la sucursal.";
                    }                                  
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = code, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [Obsolete]
        public bool PuedeDarDeBajaSucursal(int id)
        {
            try
            {
                var contacto = _context.contacto_sucursal
                              .Where(sc => sc.id_sucursal_cliente == id).ToList();

                var pedido = _context.pedido
                              .Where(sc => sc.id_sucursal_cliente == id).ToList();

                if (contacto.Count() > 0 && pedido.Count() > 0)
                    return contacto.All(sc => sc.ff_baja != null) && pedido.All(sc => sc.ff_baja != null);
                else if (contacto.Count() == 0 && pedido.Count() > 0)
                    return pedido.All(sc => sc.ff_baja != null);
                else if (contacto.Count() > 0 && pedido.Count() == 0)
                    return contacto.All(sc => sc.ff_baja != null);
                else
                    return true;
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return false;
            }
        }

        [Produces("application/json")]
        [HttpGet]
        public IActionResult BuscarSucursalesAutocomplete()
        {
            try
            {
                string term = HttpContext.Request.Query["term"].ToString();
                int id = Convert.ToInt32(HttpContext.Request.Query["id_cliente"]);
                var names = _context.sucursal_cliente
                                .Where(p => p.id_cliente.Equals(id) && p.descripcion.Contains(term))
                                .Select(p => new {
                                    id = p.id_sucursal_cliente,
                                    label = p.descripcion
                                })
                                .ToList();
                return Ok(names);
            }
            catch
            {
                return BadRequest();
            }
        }
        #endregion

        #region CONTACTO
        [HttpPost]
        [Obsolete]
        public IActionResult EditContacto([FromBody] ContactoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    if (!ModelState.IsValid)
                    {
                        Dictionary<string, int> hash = new Dictionary<string, int>(); //El hashmap sirve para ordenar las validaciones
                        hash.Add("NOMBRE", 1);
                        hash.Add("APELLIDO", 2);
                        hash.Add("IDSUCURSAL", 3);

                        var message = ModelState.Select(x => new { Key = hash[x.Key.ToUpper()], Errors = x.Value.Errors }).OrderBy(x => x.Key);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault().Errors.Select(e => e.ErrorMessage) });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var cont = param.GetContactoSucursal();
                    string msg;
                    if (param.EsBaja)
                        cont.ff_baja = DateTime.Now;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        cont.ff_alta = DateTime.Today;
                        _context.contacto_sucursal.Add(cont);
                        msg = "Contacto añadido exitosamente.";
                    }
                    else
                    {
                        ContactoSucursal c = _context.contacto_sucursal.Where(x => x.id_contacto_sucursal == cont.id_contacto_sucursal).SingleOrDefault();
                        c.nombre = cont.nombre;
                        c.apellido = cont.apellido;
                        c.id_sucursal_cliente = cont.id_sucursal_cliente;

                        /*if (cont.telefono.HasValue)
                            c.telefono = cont.telefono;
                        if (!string.IsNullOrEmpty(cont.mail))
                            c.mail = cont.mail;*/

                        _context.contacto_sucursal.Attach(c);
                        _context.Entry(c).State = EntityState.Modified;
                        msg = "Contacto actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = !param.EsBaja ? 1 : 2, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadContactos(int id_cliente)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var sucursales = (from cs in _context.contacto_sucursal
                                  join sc in _context.sucursal_cliente on cs.id_sucursal_cliente equals sc.id_sucursal_cliente
                                  where sc.id_cliente == id_cliente && cs.ff_baja == null
                                  select
                                    new
                                    {
                                        id_contacto_sucursal = cs.id_contacto_sucursal,
                                        nombre = cs.nombre,
                                        apellido = cs.apellido,
                                        sucursal = sc.descripcion
                                    });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sucursales = sucursales.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    sucursales = sucursales.Where(m => m.nombre.Contains(searchValue) 
                                                        || m.apellido.Contains(searchValue)
                                                        || m.sucursal.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = sucursales.Count();
                //Paging   
                var data = sucursales.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetContactoSucursal(int id)
        {
            try
            {
                var cs = _context.contacto_sucursal.Where(i => i.id_contacto_sucursal == id && i.ff_baja == null).FirstOrDefault();

                var contacto = cs != null ? new ContactoDTO(cs) : null;

                return Json(new { Code = cs != null ? 1 : 2, Contacto = contacto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetContactos(int id_sucursal)
        {
            try
            {
                var data = _context.contacto_sucursal
                                .Where(c => c.id_sucursal_cliente == id_sucursal && c.ff_baja == null)
                                .Select(c => new { id = c.id_contacto_sucursal, text = String.Concat(c.nombre, " " , c.apellido)})
                                .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un contacto" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarContacto(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var c = _context.contacto_sucursal.FirstOrDefault(u => u.id_contacto_sucursal == id);

                    string msg = "";
                    int code = 2;

                    if (PuedeDarDeBajaContacto(id))
                    {
                        c.ff_baja = DateTime.Now;
                        _context.contacto_sucursal.Attach(c);
                        _context.Entry(c).State = EntityState.Modified;
                        msg = "Contacto eliminado exitosamente.";
                    }
                    else{
                        msg = "No es posible eliminar el contacto.";
                        code = 4;
                    }
                        
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = code, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [Obsolete]
        public bool PuedeDarDeBajaContacto(int id)
        {
            try
            {
                var pedido = _context.pedido
                              .Where(sc => sc.id_contacto_sucursal == id).ToList();

                if (pedido.Count() > 0)
                    return pedido.All(sc => sc.ff_baja != null);
                else
                    return true;
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return false;
            }
        }
        #endregion
    }
}
