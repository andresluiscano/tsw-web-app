﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using WebApp.Models.DTO;
using System.Reflection;
using System.IO;

namespace WebApp.Controllers
{
    public class EstadoStockController : BaseController
    {
        public EstadoStockController(TSW_DB_Context _context) : base(_context) { }

        #region Estado Stock
        [Route("Parametrización/EstadoStock")]
        [Obsolete]
        public IActionResult EstadoStockView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iParametrizacion", clase = "active" });
                menu.Add(new NodoMenu() { id = "iEstado", clase = "active" });
                menu.Add(new NodoMenu() { id = "iStock", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var estados_stock = (from e in _context.estado_stock
                                     where e.ff_baja == null
                                         select e);

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    estados_stock = estados_stock.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    estados_stock = estados_stock.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = estados_stock.Count();
                //Paging   
                var data = estados_stock.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetEstadoStock(int id)
        {
            try
            {
                var e = _context.estado_stock.FirstOrDefault(u => u.id_estado_stock == id);
                var estado_stock = new EstadoStockDTO(e);
                return Json(new { Code = 1, EstadoStock = estado_stock });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditEstadoStock([FromBody] EstadoStockDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var ped = param.GetEstadoStock();
                    string msg;

                    if (!ModelState.IsValid){
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.estado_stock.Add(ped);
                        msg = "Estado añadida exitosamente.";
                    }
                    else
                    {
                        _context.estado_stock.Attach(ped);
                        _context.Entry(ped).State = EntityState.Modified;
                        msg = "Estado actualizada exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------
                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarEstadoStock(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var c = _context.estado_stock.FirstOrDefault(u => u.id_estado_stock == id);
                    c.ff_baja = DateTime.Now;
                    _context.estado_stock.Attach(c);
                    _context.Entry(c).State = EntityState.Modified;
                    var msg = "Estado eliminado exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------
                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetEstadosStock()
        {
            try
            {   
                var data = _context.estado_stock
                              .Select(c => new { id = c.id_estado_stock, text = c.descripcion })
                              .OrderBy(c=> c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un estado" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion
    }
}
