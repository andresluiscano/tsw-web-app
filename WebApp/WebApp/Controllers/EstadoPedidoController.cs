﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using WebApp.Models.DTO;
using System.Reflection;
using System.IO;

namespace WebApp.Controllers
{
    public class EstadoPedidoController : BaseController
    {
        public EstadoPedidoController(TSW_DB_Context _context) : base(_context) { }

        #region Estado Pedido

        [Route("Parametrización/EstadoPedido")]
        [Obsolete]
        public IActionResult EstadoPedidoView()
        {
            try {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iParametrizacion", clase = "active" });
                menu.Add(new NodoMenu() { id = "iEstado", clase = "active" });
                menu.Add(new NodoMenu() { id = "iPedido", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex) {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var estados_pedidos = (from e in _context.estado_pedido
                                       where e.ff_baja == null
                                       select e);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    estados_pedidos = estados_pedidos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    estados_pedidos = estados_pedidos.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = estados_pedidos.Count();
                //Paging   
                var data = estados_pedidos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetEstadoPedido(int id)
        {
            try
            {
                var e = _context.estado_pedido.FirstOrDefault(u => u.id_estado_pedido == id);
                var estado_pedido = new EstadoPedidoDTO(e);
                return Json(new { Code = 1, EstadoPedido = estado_pedido });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditEstadoPedido([FromBody] EstadoPedidoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var ped = param.GetEstadoPedido();
                    string msg;

                    if (!ModelState.IsValid) {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.estado_pedido.Add(ped);
                        msg = "Estado añadido exitosamente.";
                    }
                    else
                    {
                        _context.estado_pedido.Attach(ped);
                        _context.Entry(ped).State = EntityState.Modified;
                        msg = "Estado actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarEstadoPedido(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    string msg = "";
                    int code = 2;

                    if (PuedeDarDeBajaEstadoPedido(id))
                    {
                        var c = _context.estado_pedido.FirstOrDefault(u => u.id_estado_pedido == id);
                        c.ff_baja = DateTime.Now;
                        _context.estado_pedido.Attach(c);
                        _context.Entry(c).State = EntityState.Modified;
                        msg = "Estado eliminado exitosamente.";
                    }
                    else {
                        msg = "No es posible eliminar el contacto.";
                        code = 4;
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = code, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetEstadosPedido()
        {
            try
            {   

                var data = _context.estado_pedido
                              .Select(c => new { id = c.id_estado_pedido, text = c.descripcion })
                              .OrderBy(c=> c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un estado" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [Obsolete]
        public bool PuedeDarDeBajaEstadoPedido(int id)
        {
            try
            {
                var pedido = _context.pedido
                              .Where(sc => sc.id_estado_pedido == id).ToList();

                if (pedido.Count() > 0)
                    return pedido.All(sc => sc.ff_baja != null);
                else
                    return true;
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return false;
            }
        }

        #endregion
    }
}
