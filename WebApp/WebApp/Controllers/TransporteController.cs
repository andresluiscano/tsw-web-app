﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models.DTO;
using System.Linq.Dynamic.Core;
using WebApp.Models;


namespace WebApp.Controllers
{
    public class TransporteController : BaseController
    {
        public TransporteController(TSW_DB_Context _context) : base(_context) { }

        #region PROVEEDOR
        [Route("Proveedores/CruzDelSur")]
        [Obsolete]
        public IActionResult ProveedorView()
        {
            try
            {
                ProveedorTransporte cds = _context.proveedor_transporte.Where(c => c.id_proveedor_transporte.Equals(1) && c.ff_baja == null).FirstOrDefault();

                if (cds.logo_proveedor == null)
                    cds.logo_proveedor = "/assets/img/profile_user.jpg";

                ViewData["cds"] = cds;

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iProveedores", clase = "active" });
                menu.Add(new NodoMenu() { id = "iCruzDelSur", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProveedor(int id)
        {
            try
            {
                var p = _context.proveedor_transporte.Where(i => i.id_proveedor_transporte == id && i.ff_baja == null).FirstOrDefault();

                var prov = p != null ? new ProveedorTransporteDTO(p) : null;

                return Json(new { Code = p != null ? 1 : 2, Proveedor = prov });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditProveedor([FromBody] ProveedorTransporteDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var pro = param.GetProveedorTransporte();
                    string msg;
                    if (param.esBaja)
                        pro.ff_baja = DateTime.Now;

                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.proveedor_transporte.Add(pro);
                        msg = "Proveedor añadido exitosamente.";
                    }
                    else
                    {
                        _context.proveedor_transporte.Attach(pro);
                        _context.Entry(pro).State = EntityState.Modified;
                        msg = !param.esBaja ? "Proveedor actualizado exitosamente." : "Proveedor eliminado exitosamente."; ;
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = !param.esBaja ? 1 : 2, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProveedores()
        {
            try
            {
                var data = _context.proveedor_transporte
                              .Where(c => c.ff_baja == null)
                              .Select(c => new { id = c.id_proveedor_transporte, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un proveedor" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion

        #region SLA
        [HttpPost]
        [Obsolete]
        public IActionResult LoadSLA(int id_proveedor_transporte)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var sucursales = (from sla in _context.sla_transporte
                                  where sla.id_proveedor_transporte == id_proveedor_transporte && sla.ff_baja == null
                                  select
                                    new
                                    {
                                        id_sla_transporte = sla.id_sla_transporte,
                                        escala = sla.escala,
                                        codigo_postal = sla.codigo_postal
                                    });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    sucursales = sucursales.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    sucursales = sucursales.Where(m => m.escala.Equals(searchValue) || m.codigo_postal.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = sucursales.Count();
                //Paging   
                var data = sucursales.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditSLA([FromBody] SLATransporteDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    if (!ModelState.IsValid)
                    {
                        Dictionary<string, int> hash = new Dictionary<string, int>(); //El hashmap sirve para ordenar las validaciones
                        hash.Add("ESCALA", 1);
                        hash.Add("CODIGOPOSTAL", 2);

                        var message = ModelState.Select(x => new { Key = hash[x.Key.ToUpper()], Errors = x.Value.Errors }).OrderBy(x => x.Key);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault().Errors.Select(e => e.ErrorMessage) });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var sla = param.GetSLATransporte();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        sla.ff_alta = DateTime.Today;
                        _context.sla_transporte.Add(sla);
                        msg = "SLA añadido exitosamente.";
                    }
                    else
                    {
                        _context.sla_transporte.Attach(sla);
                        _context.Entry(sla).State = EntityState.Modified;
                        msg = "SLA actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = !param.EsBaja ? 1 : 2, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }


        [HttpGet]
        [Obsolete]
        public IActionResult GetSLA(long id)
        {
            try
            {
                //var s = _context.sucursal_cliente.Where(i => i.id_sucursal_cliente == id && i.ff_baja == null).FirstOrDefault();
                var s = (from sc in _context.sla_transporte
                         where sc.id_sla_transporte == id
                         select sc).FirstOrDefault();

                var sla = (dynamic)null;
                if (s != null) {
                    sla = new SLATransporteDTO(s);
                }               

                return Json(new { Code = s != null ? 1 : 2, Sla = sla });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetSLAs()
        {
            try
            {
                var data = _context.sla_transporte
                              .Where(c => c.ff_baja == null)
                              .Select(c => new { id = c.id_sla_transporte, text = Convert.ToString(c.escala) })
                              .OrderBy(c => c.id)
                              .ToList();

                data.Insert(0, new { id = (long)0, text = "Seleccione un SLA" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarSLA(long id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var s = _context.sla_transporte.FirstOrDefault(u => u.id_sla_transporte == id);
                    s.ff_baja = DateTime.Now;
                    _context.sla_transporte.Attach(s);
                    _context.Entry(s).State = EntityState.Modified;
                    var msg = "SLA eliminado exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }
        #endregion

        #region CIRCUITO RADIAL
        [HttpPost]
        [Obsolete]
        public IActionResult LoadCircuitosRadiales(int id_proveedor_transporte)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var circuitos = (from cr in _context.circuito_radial
                                  where cr.id_proveedor_transporte == id_proveedor_transporte && cr.ff_baja == null
                                  select cr);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    circuitos = circuitos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    circuitos = circuitos.Where(m => m.escala.Equals(searchValue) || m.valor.Equals(searchValue) || m.valor_adicional.Equals(searchValue));
                }
                //total number of rows counts   
                recordsTotal = circuitos.Count();
                //Paging   
                var data = circuitos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditCircuitoRadial([FromBody] CircuitoRadialDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    if (!ModelState.IsValid)
                    {
                        Dictionary<string, int> hash = new Dictionary<string, int>(); //El hashmap sirve para ordenar las validaciones
                        hash.Add("ESCALA", 1);
                        hash.Add("VALOR", 2);
                        hash.Add("VALORADICIONAL", 3);

                        var message = ModelState.Select(x => new { Key = hash[x.Key.ToUpper()], Errors = x.Value.Errors }).OrderBy(x => x.Key);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault().Errors.Select(e => e.ErrorMessage) });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var cr = param.GetCircuitoRadial();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        cr.ff_alta = DateTime.Today;
                        _context.circuito_radial.Add(cr);
                        msg = "Circuito radial añadido exitosamente.";
                    }
                    else
                    {
                        _context.circuito_radial.Attach(cr);
                        _context.Entry(cr).State = EntityState.Modified;
                        msg = "Circuito radial actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = !param.EsBaja ? 1 : 2, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetCircuitoRadial(int id)
        {
            try
            {
                //var s = _context.sucursal_cliente.Where(i => i.id_sucursal_cliente == id && i.ff_baja == null).FirstOrDefault();
                var cr = (from c in _context.circuito_radial
                         where c.id_circuito_radial == id
                         select c).FirstOrDefault();

                var cto = cr != null ? new CircuitoRadialDTO(cr) : null;

                return Json(new { Code = cr != null ? 1 : 2, CircuitoRadial = cto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarCircuitoRadial(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var c = _context.circuito_radial.FirstOrDefault(u => u.id_circuito_radial == id);
                    c.ff_baja = DateTime.Now;
                    _context.circuito_radial.Attach(c);
                    _context.Entry(c).State = EntityState.Modified;
                    var msg = "Circuito radial eliminado exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }
        #endregion

        [HttpGet]
        [Obsolete]
        public IActionResult GetCostoProveedorCS(long id)
        {
            try
            {
                if (id == 0)
                    return Json(new { Code = 4, Mensaje = "No se pudo calcular el transporte. Error en el ingreso de parámetros." });
                //Busco los productos del pedido
                List<PedidoProducto> productos = _context.pedido_producto.Where(x => x.id_pedido.Equals(id) && x.ff_baja == null).ToList();
                
                //Busco a Cruz del Sur
                ProveedorTransporte pt = _context.proveedor_transporte.Where(x => x.id_proveedor_transporte.Equals(1)).FirstOrDefault();

                //Busco el código postal asociado a la sucursal
                string CPSucursal = (from p in _context.pedido
                                     join sc in _context.sucursal_cliente on p.id_sucursal_cliente equals sc.id_sucursal_cliente
                                     where p.id_pedido.Equals(id)
                                     select sc).FirstOrDefault().codigo_postal;
                             

                //Busco el circuito radial en base al código postal
                CircuitoRadial CircuitoRadial = (from sla in _context.sla_transporte
                                                join cr in _context.circuito_radial on sla.escala equals cr.escala
                                                where sla.codigo_postal.Equals(CPSucursal)
                                                select cr).FirstOrDefault();

                if (CircuitoRadial == null)
                    return Json(new { Code = 4, Mensaje = "No se pudo calcular el transporte. La sucursal no tiene código postal asociado." });

                //1) Obtengo el Peso Volumetrico en CM de todos los productos y los pesos totales
                decimal VolumenTotalCM = 0;
                decimal PesoRealTotal = 0;
                int ValorUnitarioTotal = 0;

                foreach (var p in productos){
                    Producto producto = _context.producto.Where(x => p.id_producto.Equals(x.id_producto)).FirstOrDefault();
                    VolumenTotalCM += producto.volumen_cm * p.cantidad;
                    PesoRealTotal += producto.peso_real * p.cantidad;
                    ValorUnitarioTotal += producto.valor_unitario * p.cantidad;
                }

                decimal PesoVolumetricoTotal = Math.Ceiling((VolumenTotalCM / 1000000) * 350);
                decimal CalculoRadial = 0;

                //2) Comparo ambos pesos y me quedo con el mayor y luego verifico si llega a 5kg y multiplico por el valor, y valor adicional por kg
                if (PesoVolumetricoTotal > PesoRealTotal)
                    if (PesoVolumetricoTotal > 5)
                        CalculoRadial = CircuitoRadial.valor + (PesoVolumetricoTotal - 5) * CircuitoRadial.valor_adicional;
                    else
                        CalculoRadial = PesoVolumetricoTotal * CircuitoRadial.valor;
                else
                    if (PesoRealTotal > 5)
                        CalculoRadial = CircuitoRadial.valor + (PesoRealTotal - 5) * CircuitoRadial.valor_adicional;
                    else
                        CalculoRadial = PesoRealTotal * CircuitoRadial.valor;
                //3) Monto por Valor Declarado Total
                decimal MontoPorValorDeclaradoTotal = (ValorUnitarioTotal > pt.valor_maximo_permitido ? ValorUnitarioTotal : pt.valor_maximo_permitido) * (decimal)0.01;

                //4) Monto total = Calculo del Radial + Monto por Valor Declarado Total + Monto adicional por remito
                decimal MontoTotal = CalculoRadial + MontoPorValorDeclaradoTotal + pt.valor_por_remito;

                return Json(new {   Code = 1,
                                    PesoVolumetricoTotal = PesoVolumetricoTotal,
                                    PesoRealTotal = Math.Round(PesoRealTotal,2),
                                    CalculoRadial = Math.Round(CalculoRadial,2),
                                    MontoPorValorDeclaradoTotal = Math.Round(MontoPorValorDeclaradoTotal,2),
                                    ValorPorRemito = Math.Round(pt.valor_por_remito,2),
                                    MontoTotal = Math.Round(MontoTotal,2)
                                });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
    }
}
