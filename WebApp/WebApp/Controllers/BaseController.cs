﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NLog;
using WebApp.Context;

namespace WebApp.Controllers
{
    public class BaseController : Controller
    {
        public static Logger _logger = LogManager.GetCurrentClassLogger();

        public readonly TSW_DB_Context _context;

        public BaseController(TSW_DB_Context context)
        {
            _context = context;
        }
    }
}
