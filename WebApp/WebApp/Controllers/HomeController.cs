﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models.DTO;
using System.Linq.Dynamic.Core;
using WebApp.Models;
using System.Diagnostics;
using static WebApp.Models.Enum;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq.Expressions;

namespace WebApp.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(TSW_DB_Context _context) : base(_context) { }

        [Obsolete]
        public IActionResult Index()
        {
            try
            {
                List<Cliente> clientes = _context.cliente
                                      .Where(c => c.ff_baja == null)
                                      .OrderBy(c => c.id_cliente)
                                      .ToList();

                List<EstadoPedido> estados = _context.estado_pedido
                      .Where(c => c.ff_baja == null)
                      .OrderBy(c => c.id_estado_pedido)
                      .ToList();

                ViewData["clientes"] = clientes;
                ViewData["estados"] = estados;

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iPanel", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadPedidos(FiltroDTO param/*int id_cliente, string estado*/)
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data

                var pedidos = from p in _context.pedido
                                join sc in _context.sucursal_cliente 
                                    on p.id_sucursal_cliente equals sc.id_sucursal_cliente
                                where (!param.nro_pedido.HasValue || p.nro_pedido == param.nro_pedido)
                                    && (!param.nro_ticket.HasValue || p.nro_ticket == param.nro_ticket)
                                    && (!param.id_cliente.HasValue || sc.id_cliente == param.id_cliente)
                                    && (!param.id_estado_pedido.HasValue || p.id_estado_pedido == param.id_estado_pedido)
                                    && (!param.id_estado_pedido.HasValue || p.id_estado_pedido == param.id_estado_pedido)
                                    && compareDateBetween(p,param.ff_desde, param.ff_hasta, param.tipo_fecha)
                                select
                                    new
                                    {
                                        p.id_pedido,
                                        p.nro_pedido,
                                        nro_ticket = p.nro_ticket.Value > 0 ? p.nro_ticket.ToString() : "-",
                                        ff_ingreso = p.ff_ingreso.ToString("dd/MM/yyyy HH:mm"),
                                        ff_sla = (p.ff_sla.ToString() != "" ?
                                                            p.ff_sla.Value.ToString("dd/MM/yyyy") : "-"),
                                        ff_cumplimiento = (p.ff_cumplimiento.ToString() != "" ? 
                                                            p.ff_cumplimiento.Value.ToString("dd/MM/yyyy") : "-"),
                                        ff_entrega = (p.ff_entrega.HasValue ? p.ff_entrega.Value.ToString("dd/MM/yyyy") : "-"),                                       
                                        sucursal = (sc.descripcion.ToString() != "" && sc.direccion.ToString() != "" ?
                                                            $"{sc.descripcion} ({sc.direccion})" : "-"),
                                        descEstado = ((EnumEstadoPedido)p.id_estado_pedido).ToString(),
                                    };

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    pedidos = pedidos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    pedidos = pedidos.Where(m => m.nro_ticket.Equals(searchValue));
                }
                //total number of rows counts   
                recordsTotal = pedidos.Count();
                //Paging   
                var data = pedidos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        /*private int[] ObtenerFiltroEstados(string estado) {
            switch (estado)
            {
                case "I":
                    return new int[2] { (int)EnumEstadoPedido.Ingresado, (int)EnumEstadoPedido.Suspendido };
                case "C":
                    return new int[3] { (int)EnumEstadoPedido.EnTransito, (int)EnumEstadoPedido.Demorado, (int)EnumEstadoPedido.Preparado};
                default:
                    return new int[2] { (int)EnumEstadoPedido.Retornado, (int)EnumEstadoPedido.Entregado };            
            }
        }*/

        private bool compareDateBetween(Pedido p, DateTime? ff_desde, DateTime? ff_hasta, int? tipo_fecha) {
            if (tipo_fecha.HasValue)
                switch (tipo_fecha)
                {
                    case 1: //INGRESO
                        if (ff_desde.HasValue && ff_hasta.HasValue)
                            return (p.ff_ingreso >= ff_desde.Value.Date && p.ff_ingreso <= ff_hasta.Value.Date);
                        else
                            return true;
                    case 2: //ENTREGA
                        if (ff_desde.HasValue && ff_hasta.HasValue)
                            return (p.ff_entrega >= ff_desde.Value.Date && p.ff_entrega <= ff_hasta.Value.Date);
                        else
                            return true;
                    case 3: //SLA
                        if (ff_desde.HasValue && ff_hasta.HasValue)
                            return (p.ff_sla >= ff_desde.Value.Date && p.ff_sla <= ff_hasta.Value.Date);
                        else
                            return true;
                    case 4: //CUMPLIMIENTO
                        if (ff_desde.HasValue && ff_hasta.HasValue)
                            return (p.ff_cumplimiento >= ff_desde.Value.Date && p.ff_cumplimiento <= ff_hasta.Value.Date);
                        else
                            return true;
                    default:
                        return true;
                }
            else
                return true;
        }

        /*[HttpGet]
        [Obsolete]
        public IActionResult ObtenerTotales(int id_cliente)
        {
            try
            {
                var productos = (from p in _context.pedido
                                join sc in _context.sucursal_cliente on p.id_sucursal_cliente equals sc.id_sucursal_cliente
                                where sc.id_cliente == id_cliente
                                select p.id_estado_pedido).ToList();

                var data = new { 
                                 Nuevos = productos.Count(p => ObtenerFiltroEstados("I").All(k => k == p)),
                                 EnCurso = productos.Count(p => ObtenerFiltroEstados("C").Any(k => k == p)),
                                 Finalizados = productos.Count(p => ObtenerFiltroEstados("F").Any(k => k == p))
                                };

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }*/

        [HttpPost]
        [Obsolete]
        public IActionResult RetornarPedido(long id, string comentarios)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (string.IsNullOrEmpty(comentarios))
                        return Json(new { Code = 4, Mensaje = "Ingrese un comentario." });
                    //-----------------------------------------------------------------------------------------------
                    var p = _context.pedido.FirstOrDefault(u => u.id_pedido == id);
                    p.id_estado_pedido = (int)EnumEstadoPedido.Rechazado;
                    p.ff_baja = DateTime.Now;
                    p.comentarios = comentarios;
                    _context.pedido.Attach(p);
                    _context.Entry(p).State = EntityState.Modified;
                    var msg = "Pedido retornado exitosamente.";
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------
                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

    }
}
