﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using WebApp.Models.DTO;
using System.Reflection;
using System.IO;

namespace WebApp.Controllers
{
    public class ProvinciaController : BaseController
    {
        public ProvinciaController(TSW_DB_Context _context) : base(_context) { }

        #region Provincia
        [Obsolete]
        [Route("Parametrizacion/Provincia")]
        public IActionResult ProvinciaView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iParametrizacion", clase = "active" });
                menu.Add(new NodoMenu() { id = "iProvincia", clase = "active" });

                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var provincias = (from p in _context.provincia
                                  select p);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    provincias = provincias.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    provincias = provincias.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = provincias.Count();
                //Paging   
                var data = provincias.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProvincia(int id)
        {
            try
            {
                var p = _context.provincia.FirstOrDefault(u => u.id_provincia == id);
                var provincia = new ProvinciaDTO(p);
                return Json(new { Code = 1, Provincia = provincia });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditProvincia([FromBody] ProvinciaDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var ped = param.GetProvincia();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.provincia.Add(ped);
                        msg = "Provincia añadida exitosamente.";
                    }
                    else
                    {
                        _context.provincia.Attach(ped);
                        _context.Entry(ped).State = EntityState.Modified;
                        msg = "Provincia actualizada exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProvincias()
        {
            try
            {   

                var data = _context.provincia
                              .Select(c => new { id = c.id_provincia, text = c.descripcion })
                              .OrderBy(c=> c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione una provincia" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion
    }
}
