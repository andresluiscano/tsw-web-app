﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models.DTO;
using System.Linq.Dynamic.Core;
using WebApp.Models;
using static WebApp.Models.Enum;

namespace WebApp.Controllers
{
    public class PedidoController : BaseController
    {
        public PedidoController(TSW_DB_Context _context) : base(_context) { }

        [Route("DetallePedido/{id}")]
        [HttpGet]
        [Obsolete]
        //public IActionResult DetallePedidoView(long id)
        public async Task<ActionResult>DetallePedidoView(long id)
        {
            try
            {
                var pedido = _context.pedido.Where(x => x.id_pedido == id).FirstOrDefault();

                if (pedido != null)
                {
                    ViewData["IdPedido"] = pedido.id_pedido;
                    ViewData["NroPedido"] = pedido.nro_pedido;
                    ViewData["NroTicket"] = pedido.nro_ticket.Value > 0 ? pedido.nro_ticket.ToString() : "";
                    ViewData["Comentarios"] = pedido.comentarios;
                    ViewData["EsNeutral"] = pedido.es_neutral == 1 ? "Sí" : "No";
                    ViewData["FFIngreso"] = pedido.ff_ingreso.ToString("dd/MM/yyyy HH:mm").Split(" ")[0];
                    ViewData["HHIngreso"] = pedido.ff_ingreso.ToString("dd/MM/yyyy HH:mm").Split(" ")[1];
                    ViewData["FFSLA"] = pedido.ff_sla.HasValue ? pedido.ff_sla.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["FFCumplimiento"] = pedido.ff_cumplimiento.HasValue ? pedido.ff_cumplimiento.Value.ToString("dd/MM/yyyy") : "";
                    ViewData["EnvioDiferido"] = pedido.ff_envio_diferido.HasValue ? "Sí" : "No";
                    ViewData["FFEnvioDiferido"] = pedido.ff_envio_diferido.HasValue ? pedido.ff_envio_diferido.Value.ToString("dd/MM/yyyy") : "";
                    
                    var sc = _context.sucursal_cliente.Where(x => pedido.id_sucursal_cliente == x.id_sucursal_cliente);
                    ViewData["Sucursal"] = pedido.id_sucursal_cliente.Value > 0 ? 
                                            sc.Select(x => new { Sucursal = string.Format("{0} ({1})", x.descripcion, x.direccion) })
                                            .FirstOrDefault().Sucursal : " ";

                    ViewData["Contacto"] = pedido.id_contacto_sucursal.Value > 0 ? _context.contacto_sucursal
                                            .Where(x => pedido.id_contacto_sucursal == x.id_contacto_sucursal)
                                            .Select(x => new { Contacto = string.Format("{0} {1}", x.nombre, x.apellido) })
                                            .FirstOrDefault().Contacto : " ";

                    ViewData["TelefonoContacto"] = pedido.telefono_contacto.Value > 0 ? pedido.telefono_contacto.ToString() : "";
                    ViewData["IdEquipo"] = pedido.id_equipo;
                    ViewData["ObservacionesEstado"] = pedido.observaciones_estado;


                    ViewData["ProveedorTransporte"] = pedido.id_proveedor_transporte.Value > 0 ?  _context.proveedor_transporte
                                                        .Where(x => pedido.id_proveedor_transporte == x.id_proveedor_transporte)
                                                        .FirstOrDefault().descripcion : " ";

                    ViewData["Estado"] = pedido.id_estado_pedido;
                    ViewData["Cliente"] = pedido.id_sucursal_cliente.Value > 0 ? _context.cliente
                                            .Where(x => sc.FirstOrDefault().id_cliente == x.id_cliente).FirstOrDefault().descripcion : " ";

                    var productos = from pp in _context.pedido_producto
                                    join p in _context.producto on pp.id_producto equals p.id_producto
                                    where pp.id_pedido == pedido.id_pedido && pp.ff_baja == null
                                    orderby pp.id_pedido_producto
                                    select new DescripcionProducto()
                                    {
                                        IdPedidoProducto = pp.id_pedido_producto,
                                        Producto = $"{p.descripcion} " +
                                                    $"(Marca: {_context.marca_producto.Where(map => map.id_marca_producto.Equals(p.id_marca_producto)).FirstOrDefault().descripcion} - " +
                                                    $"Modelo: {_context.modelo_producto.Where(mop => mop.id_modelo_producto.Equals(p.id_modelo_producto)).FirstOrDefault().descripcion})",
                                        /*Marca = pp.id_marca_producto > 0 ?
                                                            _context.marca_producto.Where(map => map.id_marca_producto.Equals(pp.id_marca_producto)).FirstOrDefault().descripcion :
                                                            "No aplica",
                                        Modelo = pp.id_modelo_producto > 0 ?
                                                            _context.modelo_producto.Where(mop => mop.id_modelo_producto.Equals(pp.id_modelo_producto)).FirstOrDefault().descripcion :
                                                            "No aplica",*/
                                        Configuracion = pp.id_configuracion_producto > 0 ?
                                                            _context.configuracion_producto.Where(cp => cp.id_configuracion_producto.Equals(pp.id_configuracion_producto)).FirstOrDefault().descripcion :
                                                            "No aplica",
                                        Cantidad = pp.cantidad,
                                        CantidadRetorno = pp.cantidad_retorno,
                                    };

                    ViewData["Productos"] = productos;

                    List<EstadoPedido> estados = _context.estado_pedido
                                                          .Where(c => c.ff_baja == null)
                                                          .OrderBy(c => c.id_estado_pedido)
                                                          .ToList();
                    ViewData["estados"] = estados;

                    if (productos != null)
                        ViewData["CostoProveedorCS"] = GetCostoProveedorCS(pedido.id_pedido);



                }

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iPanel", clase = "active" });
                ViewData["NodoMenu"] = menu;

                //return View();
                return await Task.Run(() => View());
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [Route("NuevoPedido")]
        [HttpGet]
        [Obsolete]
        public IActionResult NuevoPedidoView() {
            try
            {
                List<Cliente> clientes = _context.cliente
                      .Where(c => c.ff_baja == null)
                      .OrderBy(c => c.id_cliente)
                      .ToList();

                List<EstadoPedido> estados = _context.estado_pedido
                      .Where(c => c.ff_baja == null)
                      .OrderBy(c => c.id_estado_pedido)
                      .ToList();

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iPanel", clase = "active" });
                ViewData["NodoMenu"] = menu;
                ViewData["clientes"] = clientes;
                ViewData["estados"] = estados;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetffSLA(string FFIngreso, string HHIngreso, int idSuc, int dias)
        {
            try
            {

                if (FFIngreso == null || HHIngreso == null || idSuc == 0)
                    return Json(new { Code = 4, Mensaje = "No se pudo calcular la fecha de SLA. Error en el ingreso de parámetros." });

                    //PASO 1: Buscamos el SLA de la sucursal dependiendo la localidad.

                    int sla = 0;

                    if (dias == 0)
                        sla = (from sc in _context.sucursal_cliente
                               join cps in _context.codigo_postal_sla on sc.codigo_postal equals cps.codigo_postal.ToString()
                               where sc.id_sucursal_cliente == idSuc
                               select cps.sla).FirstOrDefault();
                    else
                        sla = dias;

                    if (sla == 0)
                        return Json(new { Code = 4, Mensaje = "No se pudo calcular la fecha de SLA automáticamente. No existe SLA para el código postal de la sucursal seleccionada. Ingrese un SLA manual." });

                    //PASO 2: Parseamos hora y fecha.                  
                    DateTime ff_ing = DateTime.ParseExact(FFIngreso, "dd/MM/yyyy", null);//Convert.ToDateTime(FFIngreso);
                    TimeSpan hh_ing = TimeSpan.Parse(HHIngreso);

                    //Si la hora de ingreso del pedido es mayor a las 16 hs entonces, se cuenta a partir del día siguiente, o sea, se suma 1 al SLA.
                    var superoHorario = hh_ing > new TimeSpan(16, 0, 0);
                    /*if (hh_ing > new TimeSpan(16, 0, 0))
                        sla++;*/

                    string ff_sla = DateTimeExtensions.AddWorkdays(ff_ing, superoHorario ? sla+1 : sla, _context).ToString("dd/MM/yyyy");
                    return Json(new { Code = 1, ff_sla = ff_sla, sla = sla });


            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditPedido(PedidoDTO param, List<PedidoProductoDTO> productos, bool saltarValidacion)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    /*if (ModelState.IsValid)
                    {
                        Dictionary<string, int> hash = new Dictionary<string, int>(); //El hashmap sirve para ordenar las validaciones
                        hash.Add("NROPEDIDO", 2);
                        hash.Add("IDSUCURSALCLIENTE", 2);
                        hash.Add("IDCONTACTOSUCURSAL", 3);
                        hash.Add("ESNEUTRAL", 4);
                        hash.Add("NROTICKET", 2);
                        hash.Add("IDPROVEEDORTRANSPORTE", 6);

                        var message = ModelState.Select(x => new { Key = hash[x.Key.ToUpper()], Errors = x.Value.Errors }).OrderBy(x => x.Key);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault().Errors.Select(e => e.ErrorMessage) });
                    }*/
                    //-----------------------------------------------------------------------------------------------
                    var p = param.GetPedido();
                    string msg;         
                    //-----------------------------------------------------------------------------------------------   
                    //Validación duplicado
                    if (_context.pedido.Where(x=> x.nro_pedido == p.nro_pedido).FirstOrDefault() != null)
                        return Json(new
                        {
                            Code = 4,
                            Mensaje = "No se pudo crear un nuevo pedido. Esta intentando agregar un número de pedido duplicado.",
                            DatosPedido = new { NroPedido = p.nro_pedido }
                        });

                    if (!saltarValidacion){
                        //Aviso de nro pedido correlativo
                        long nro_correlativo = 0;
                        if (p.id_sucursal_cliente.Value > 0 && _context.pedido.Count() > 0)
                            nro_correlativo = (Convert.ToInt64(_context.pedido.OrderByDescending(x => x.nro_pedido)
                                                                        .Select(x => x.nro_pedido)
                                                                        .First()) + 1);

                        if (nro_correlativo > 0 && nro_correlativo != p.nro_pedido)
                            return Json(new
                            {
                                Code = 5,
                                Mensaje = "El Nro. TSW no es correlativo con el último pedido del cliente para esa sucursal.",
                                DatosPedido = new { NroPedido = p.nro_pedido, NroCorrelativo = nro_correlativo }
                            });
                    }
                    
                    if (param.EsNuevo())
                    {
                        if (p.id_estado_pedido == 0)
                            if (p.id_sucursal_cliente.HasValue)
                                p.id_estado_pedido = (int)EnumEstadoPedido.Ingresado;
                            else
                                p.id_estado_pedido = (int)EnumEstadoPedido.Suspendido;

                        p.ff_alta = DateTime.Now;
                        _context.pedido.Add(p);
                        msg = "Pedido añadido exitosamente.";
                    }
                    else
                    {
                        _context.pedido.Attach(p);
                        _context.Entry(p).State = EntityState.Modified;
                        msg ="Pedido actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    //-----------------------------------------------------------------------------------------------
                    foreach (var item in productos)
                    {
                        var producto = item.GetPedidoProducto();
                        producto.id_pedido = p.id_pedido;
                        producto.ff_alta = DateTime.Now;
                        _context.pedido_producto.Add(producto);
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------
                    return Json(new { Code = 1, 
                                      Mensaje = msg, 
                                      DatosPedido = new {NroPedido = p.nro_pedido, FFSLA = p.ff_sla.HasValue ? p.ff_sla.Value.ToString("dd/MM/yyyy") : "" }
                    });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult GuardarPedido(long id_pedido, List<PedidoProductoDTO> productos, int id_estado_pedido)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    /*if (productos.Count == 0)
                        return Json(new { Code = 4, Mensaje = "No se puede guardar un pedido sin productos." });*/
                    //-----------------------------------------------------------------------------------------------
                    if (productos.Count == 0)
                        foreach (var item in productos)
                        {
                            var producto = item.GetPedidoProducto();
                            producto.id_pedido = id_pedido;
                            producto.ff_alta = DateTime.Now;
                            _context.pedido_producto.Add(producto);
                        }
                    //-----------------------------------------------------------------------------------------------
                    Pedido p = _context.pedido.FirstOrDefault(x => x.id_pedido.Equals(id_pedido));
                    p.id_estado_pedido = id_estado_pedido;
                    _context.pedido.Attach(p);
                    _context.Entry(p).State = EntityState.Modified;

                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------
                    return Json(new
                    {
                        Code = 1,
                        Mensaje = "El pedido ha sido guardado exitosamente.",                        
                    });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarProducto(long id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    //Validaciones
                    string msg = "";
                    int code = 0;
                    if (id == 0) {
                        msg = "Ocurrió un error en el envío de parámetros.";
                        code = 2;
                    } 
                    else {
                        var pp = _context.pedido_producto.Where(x => x.id_pedido_producto == id).FirstOrDefault();
                        pp.ff_baja = DateTime.Now;
                        _context.pedido_producto.Attach(pp);
                        //-----------------------------------------------------------------------------------------------
                        msg = "Producto eliminado exitosamente.";
                        code = 1;
                        //-----------------------------------------------------------------------------------------------
                        _context.SaveChanges();
                        transaction.Commit();
                        //-----------------------------------------------------------------------------------------------
                    }
                    return Json(new
                    {
                        Code = code,
                        Mensaje = msg,
                    });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message, Excepcion = ex.InnerException.Message.ToString() });
                }
            }
        }

        //[HttpGet]
        [Obsolete]
        //public IActionResult GetCostoProveedorCS(long id)
        public CostoProveedorCS GetCostoProveedorCS(long id)
        {
            try
            {
                if (id == 0)
                    //return Json(new { Code = 4, Mensaje = "No se pudo calcular el transporte. Error en el ingreso de parámetros." });
                    return (CostoProveedorCS)null;
                //Busco los productos del pedido
                List<PedidoProducto> productos = _context.pedido_producto.Where(x => x.id_pedido.Equals(id) && x.ff_baja == null).ToList();

                //Busco a Cruz del Sur
                ProveedorTransporte pt = _context.proveedor_transporte.Where(x => x.id_proveedor_transporte.Equals(1)).FirstOrDefault();

                //Busco el código postal asociado a la sucursal
                string CPSucursal = (from p in _context.pedido
                                     join sc in _context.sucursal_cliente on p.id_sucursal_cliente equals sc.id_sucursal_cliente
                                     where p.id_pedido.Equals(id)
                                     select sc).FirstOrDefault().codigo_postal;


                //Busco el circuito radial en base al código postal
                CircuitoRadial CircuitoRadial = (from sla in _context.sla_transporte
                                                 join cr in _context.circuito_radial on sla.escala equals cr.escala
                                                 where sla.codigo_postal.Equals(CPSucursal)
                                                 select cr).FirstOrDefault();

                if (CircuitoRadial == null)
                    //return Json(new { Code = 4, Mensaje = "No se pudo calcular el transporte. La sucursal no tiene código postal asociado." });
                    return (CostoProveedorCS)null;

                //1) Obtengo el Peso Volumetrico en CM de todos los productos y los pesos totales
                decimal VolumenTotalCM = 0;
                decimal PesoRealTotal = 0;
                int ValorUnitarioTotal = 0;

                foreach (var p in productos)
                {
                    Producto producto = _context.producto.Where(x => p.id_producto.Equals(x.id_producto)).FirstOrDefault();
                    VolumenTotalCM += producto.volumen_cm * p.cantidad;
                    PesoRealTotal += producto.peso_real * p.cantidad;
                    ValorUnitarioTotal += producto.valor_unitario * p.cantidad;
                }

                decimal PesoVolumetricoTotal = Math.Ceiling((VolumenTotalCM / 1000000) * 350);
                decimal CalculoRadial = 0;

                //2) Comparo ambos pesos y me quedo con el mayor y luego verifico si llega a 5kg y multiplico por el valor, y valor adicional por kg
                if (PesoVolumetricoTotal > PesoRealTotal)
                    if (PesoVolumetricoTotal > 5)
                        CalculoRadial = CircuitoRadial.valor + (PesoVolumetricoTotal - 5) * CircuitoRadial.valor_adicional;
                    else
                        CalculoRadial = PesoVolumetricoTotal * CircuitoRadial.valor;
                else
                    if (PesoRealTotal > 5)
                    CalculoRadial = CircuitoRadial.valor + (PesoRealTotal - 5) * CircuitoRadial.valor_adicional;
                else
                    CalculoRadial = PesoRealTotal * CircuitoRadial.valor;
                //3) Monto por Valor Declarado Total
                decimal MontoPorValorDeclaradoTotal = (ValorUnitarioTotal > pt.valor_maximo_permitido ? ValorUnitarioTotal : pt.valor_maximo_permitido) * (decimal)0.01;

                //4) Monto total = Calculo del Radial + Monto por Valor Declarado Total + Monto adicional por remito
                decimal MontoTotal = CalculoRadial + MontoPorValorDeclaradoTotal + pt.valor_por_remito;

                CostoProveedorCS cpCS = new CostoProveedorCS();
                cpCS.PesoVolumetricoTotal = PesoVolumetricoTotal;
                cpCS.MontoPorValorDeclaradoTotal = Math.Round(MontoPorValorDeclaradoTotal, 2);
                cpCS.PesoRealTotal = Math.Round(PesoRealTotal, 2);
                cpCS.CalculoRadial = Math.Round(CalculoRadial, 2);
                cpCS.ValorPorRemito = Math.Round(pt.valor_por_remito, 2);
                cpCS.MontoTotal = Math.Round(MontoTotal, 2);

                return cpCS;
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return (CostoProveedorCS)null; //Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

    }

    public static class DateTimeExtensions
    {
        [Obsolete]
        public static DateTime AddWorkdays(this DateTime originalDate, int workDays, TSW_DB_Context _context)
        {
            var feriados = _context.feriado.Select(f => f.ff_feriado.ToString("dd/MM/yyyy")).ToList();
            DateTime tmpDate = originalDate;
            while (workDays > 0)
            {
                tmpDate = tmpDate.AddDays(1);
                if (tmpDate.DayOfWeek < DayOfWeek.Saturday &&
                    tmpDate.DayOfWeek > DayOfWeek.Sunday &&
                    !feriados.Contains(tmpDate.ToString("dd/MM/yyyy")))
                    workDays--;
            }
            return tmpDate;
        }
    }
}