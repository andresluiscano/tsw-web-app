﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using WebApp.Models.DTO;
using System.Reflection;
using System.IO;

namespace WebApp.Controllers
{
    public class CodigoPostalSLAController : BaseController
    {
        public CodigoPostalSLAController(TSW_DB_Context _context) : base(_context) { }

        [Obsolete]
        [Route("Parametrizacion/SLA")]
        public IActionResult CodigoPostalSLAView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iParametrizacion", clase = "active" });
                menu.Add(new NodoMenu() { id = "iCodigoPostalSLA", clase = "active" });

                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var slas = (from p in _context.codigo_postal_sla
                            where p.ff_baja == null
                            select p);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    slas = slas.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    slas = slas.Where(m => m.codigo_postal.Equals(searchValue) || m.sla.Equals(searchValue));
                }
                //total number of rows counts   
                recordsTotal = slas.Count();
                //Paging   
                var data = slas.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditCodigoPostalSLA([FromBody] CodigoPostalSLADTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var cpsla = param.GetCodigoPostalSLA();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        cpsla.ff_alta = DateTime.Now;
                        _context.codigo_postal_sla.Add(cpsla);
                        msg = "SLA añadido exitosamente.";
                    }
                    else
                    {
                        _context.codigo_postal_sla.Attach(cpsla);
                        _context.Entry(cpsla).State = EntityState.Modified;
                        msg = "SLA actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }

        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetCodigoPostalSLA(int id)
        {
            try
            {
                var cpsla = _context.codigo_postal_sla.FirstOrDefault(u => u.id_codigo_postal_sla == id);
                var cpslaDTO = new CodigoPostalSLADTO(cpsla);
                return Json(new { Code = 1, CodigoPostalSLA = cpslaDTO });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

    }
}
