﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Context;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using WebApp.Models.DTO;
using System.Reflection;
using System.IO;
using System.Globalization;

namespace WebApp.Controllers
{
    public class ProductoController : BaseController
    {

        public ProductoController(TSW_DB_Context _context) : base(_context) { }

        #region PRODUCTO
        [Route("Productos/Producto")]
        [Obsolete]
        public IActionResult ProductoView()
        {
            try
            {
                List<MarcaProducto> marcas = _context.marca_producto
                     .Where(c => c.ff_baja == null)
                     .OrderBy(c => c.id_marca_producto)
                     .ToList();

                List<ModeloProducto> modelos = _context.modelo_producto
                     .Where(c => c.ff_baja == null)
                     .OrderBy(c => c.id_modelo_producto)
                     .ToList();

                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iProductos", clase = "active" });
                menu.Add(new NodoMenu() { id = "iProducto", clase = "active" });
                ViewData["NodoMenu"] = menu;
                ViewData["marcas"] = marcas;
                ViewData["modelos"] = modelos;
                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var productos = (from p in _context.producto
                                 join ma in _context.marca_producto on p.id_marca_producto equals ma.id_marca_producto
                                 join mo in _context.modelo_producto on p.id_modelo_producto equals mo.id_modelo_producto
                                 where p.ff_baja == null
                                    select new {
                                        p.id_producto,
                                        p.descripcion,
                                        marca_descripcion = ma.descripcion,
                                        modelo_descripcion = mo.descripcion,
                                        p.volumen_cm,
                                        p.peso_real,
                                        p.valor_unitario,
                                        p.observaciones
                                    });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    productos = productos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    productos = productos.Where(m => m.marca_descripcion.Contains(searchValue)
                                                    || m.descripcion.Contains(searchValue)
                                                    || m.modelo_descripcion.Contains(searchValue)
                                                    || m.volumen_cm.Equals(searchValue)
                                                    || m.peso_real.Equals(searchValue)
                                                    || m.valor_unitario.Equals(searchValue)
                                                    || m.observaciones.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = productos.Count();
                //Paging   
                var data = productos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditProducto([FromBody] ProductoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var prod = param.GetProducto();
                    string msg;

                    if (!ModelState.IsValid)
                        return Json(new { Code = 2, Mensaje = "Por favor verifique los campos ingresados." });
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.producto.Add(prod);
                        msg = "Producto añadido exitosamente.";
                    }
                    else
                    {
                        _context.producto.Attach(prod);
                        _context.Entry(prod).State = EntityState.Modified;
                        msg = "Producto actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarProducto(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var p = _context.producto.FirstOrDefault(u => u.id_producto == id);
                    p.ff_baja = DateTime.Now;
                    _context.producto.Attach(p);
                    _context.Entry(p).State = EntityState.Modified;
                    var msg = "Producto eliminado exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProductos()
        {
            try
            {

                /*var data = _context.producto
                              .Where(p => p.ff_baja == null)
                              .Select(c => new { id = c.id_producto, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();*/
                var data = (from p in _context.producto
                            join ma in _context.marca_producto on p.id_marca_producto equals ma.id_marca_producto
                            join mo in _context.modelo_producto on p.id_modelo_producto equals mo.id_modelo_producto
                            where p.ff_baja == null
                            orderby p.id_producto
                            select new
                            { id = p.id_producto, text = $"{p.descripcion} (Marca: {ma.descripcion } - Modelo: {mo.descripcion})" }).ToList();

                data.Insert(0, new { id = 0, text = "Seleccione un producto" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetProducto(int id)
        {
            try
            {
                var p = _context.producto.FirstOrDefault(u => u.id_producto == id);
                var producto = new ProductoDTO(p);
                return Json(new { Code = 1, Producto = producto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion

        #region MODELO
        [Route("Productos/Modelo")]
        [Obsolete]
        public IActionResult ModeloView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iProductos", clase = "active" });
                menu.Add(new NodoMenu() { id = "iModelo", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadDataModelo()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var productos = (from p in _context.modelo_producto
                                 where p.ff_baja == null
                                 select p);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    productos = productos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    productos = productos.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = productos.Count();
                //Paging   
                var data = productos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditModelo([FromBody] ModeloProductoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var prod = param.GetModeloProducto();
                    string msg;

                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.modelo_producto.Add(prod);
                        msg = "Modelo añadido exitosamente.";
                    }
                    else
                    {
                        _context.modelo_producto.Attach(prod);
                        _context.Entry(prod).State = EntityState.Modified;
                        msg = "Modelo actualizado exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarModelo(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var p = _context.modelo_producto.FirstOrDefault(u => u.id_modelo_producto == id);
                    p.ff_baja = DateTime.Now;
                    _context.modelo_producto.Attach(p);
                    _context.Entry(p).State = EntityState.Modified;
                    var msg = "Modelo eliminado exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetModelos()
        {
            try
            {

                var data = _context.modelo_producto
                              .Where(p => p.ff_baja == null)
                              .Select(c => new { id = c.id_modelo_producto, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();


                data.Insert(0, new { id = 0, text = "Seleccione un modelo" });
                data.Insert(1, new { id = -1, text = "No aplica" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetModelo(int id)
        {
            try
            {
                var mp = _context.modelo_producto.FirstOrDefault(u => u.id_modelo_producto == id);
                var modelo_producto = new ModeloProductoDTO(mp);
                return Json(new { Code = 1, ModeloProducto = modelo_producto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion

        #region CONFIGURACION
        [Route("Productos/Configuracion")]
        [Obsolete]
        public IActionResult ConfiguracionView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iProductos", clase = "active" });
                menu.Add(new NodoMenu() { id = "iConfiguracion", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadDataConfiguracion()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var productos = (from p in _context.configuracion_producto
                                 where p.ff_baja == null
                                 select p);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    productos = productos.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    productos = productos.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = productos.Count();
                //Paging   
                var data = productos.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditConfiguracion([FromBody] ConfiguracionProductoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    var conf = param.GetConfiguracionProducto();
                    string msg;
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.configuracion_producto.Add(conf);
                        msg = "Configuración añadida exitosamente.";
                    }
                    else
                    {
                        _context.configuracion_producto.Attach(conf);
                        _context.Entry(conf).State = EntityState.Modified;
                        msg = "Configuración actualizada exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarConfiguracion(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var c = _context.configuracion_producto.FirstOrDefault(u => u.id_configuracion_producto == id);
                    c.ff_baja = DateTime.Now;
                    _context.configuracion_producto.Attach(c);
                    _context.Entry(c).State = EntityState.Modified;
                    var msg = "Configuración eliminada exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetConfiguraciones()
        {
            try
            {

                var data = _context.configuracion_producto
                              .Where(p => p.ff_baja == null)
                              .Select(c => new { id = c.id_configuracion_producto, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();

                data.Insert(0, new { id = 0, text = "Seleccione una configuración" });
                data.Insert(1, new { id = -1, text = "No aplica" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetConfiguracion(int id)
        {
            try
            {
                var c = _context.configuracion_producto.FirstOrDefault(u => u.id_configuracion_producto == id);
                var configuracion_producto = new ConfiguracionProductoDTO(c);
                return Json(new { Code = 1, ConfiguracionProducto = configuracion_producto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion

        #region MARCA
        [Route("Productos/Marca")]
        [Obsolete]
        public IActionResult MarcaView()
        {
            try
            {
                List<NodoMenu> menu = new List<NodoMenu>();
                menu.Add(new NodoMenu() { id = "iProductos", clase = "active" });
                menu.Add(new NodoMenu() { id = "iMarca", clase = "active" });
                ViewData["NodoMenu"] = menu;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return RedirectToAction("Error404View", "Error");
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult LoadDataMarca()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skip number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction (asc, desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                //Paging Size (10, 20, 50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                // getting all Customer data  
                var marcas = (from p in _context.marca_producto
                              where p.ff_baja == null
                              select p);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    marcas = marcas.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    marcas = marcas.Where(m => m.descripcion.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = marcas.Count();
                //Paging   
                var data = marcas.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }

        }

        [HttpPost]
        [Obsolete]
        public IActionResult EditMarca([FromBody] MarcaProductoDTO param)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var marc = param.GetMarcaProducto();
                    string msg;

                    if (!ModelState.IsValid)
                    {
                        var message = ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage);

                        return Json(new { Code = 4, Mensaje = message.FirstOrDefault() });
                    }
                    //-----------------------------------------------------------------------------------------------
                    if (param.EsNuevo())
                    {
                        _context.marca_producto.Add(marc);
                        msg = "Marca añadida exitosamente.";
                    }
                    else
                    {
                        _context.marca_producto.Attach(marc);
                        _context.Entry(marc).State = EntityState.Modified;
                        msg = "Marca actualizada exitosamente.";
                    }
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpPost]
        [Obsolete]
        public IActionResult EliminarMarca(int id)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //-----------------------------------------------------------------------------------------------
                    var p = _context.marca_producto.FirstOrDefault(u => u.id_marca_producto == id);
                    p.ff_baja = DateTime.Now;
                    _context.marca_producto.Attach(p);
                    _context.Entry(p).State = EntityState.Modified;
                    var msg = "Marca eliminada exitosamente."; ;
                    //-----------------------------------------------------------------------------------------------
                    _context.SaveChanges();
                    transaction.Commit();
                    //-----------------------------------------------------------------------------------------------

                    return Json(new { Code = 1, Mensaje = msg });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                    return Json(new { Code = 3, Mensaje = ex.Message });
                }
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetMarcas()
        {
            try
            {

                var data = _context.marca_producto
                              .Where(p => p.ff_baja == null)
                              .Select(c => new { id = c.id_marca_producto, text = c.descripcion })
                              .OrderBy(c => c.id)
                              .ToList();


                data.Insert(0, new { id = 0, text = "Seleccione una marca" });
                data.Insert(1, new { id = -1, text = "No aplica" });

                return Json(new { Code = 1, data });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }

        [HttpGet]
        [Obsolete]
        public IActionResult GetMarca(int id)
        {
            try
            {
                var mp = _context.marca_producto.FirstOrDefault(u => u.id_marca_producto == id);
                var marca_producto = new MarcaProductoDTO(mp);
                return Json(new { Code = 1, MarcaProducto = marca_producto });
            }
            catch (Exception ex)
            {
                _logger.LogException(NLog.LogLevel.Error, "Ha ocurrido una excepción.", ex);
                return Json(new { Code = 3, Mensaje = ex.Message });
            }
        }
        #endregion

    }
}
